FROM node:alpine3.14

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Installing dependencies
COPY package*.json /usr/src/app/
RUN yarn

# Copying source files
COPY . /usr/src/app

# Build the app
RUN yarn run build

# Expose the port
EXPOSE 5000

# Running the API
CMD "yarn" "run" "serve" "--host" "--port" "5000"
