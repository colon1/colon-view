import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';

export default defineConfig({
  plugins: [solidPlugin()],
  publicDir: 'public',
  base: '/colon-view/',
  resolve: {
    // https://github.com/vitejs/vite/issues/279#issuecomment-815532835
    alias: [
      { find: '@public', replacement: '/public' },
      { find: '@common', replacement: '/src/common' },
    ],
  },
  server: {
    watch: {
      usePolling: true,
    },
  },
});
