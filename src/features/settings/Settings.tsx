import { Accessor, createEffect, on } from 'solid-js';
import Flex from '@common/components/wrapper/Flex';
import Switch from '@common/components/switch/Switch';
import Slider from '@common/components/slider/Slider';
import {
  getGrid,
  getOwnership,
  getNotifications,
  getVolume,
  setGrid,
  setOwnership,
  setNotifications,
  setVolume,
} from './store';

// extra SFX

import { switchOn, switchOff } from '../audio';

let firstTime = true;
const applySwitchSFX = (accessor: Accessor<unknown>) => {
  createEffect(
    on(accessor, (activated) => {
      if (!firstTime) (activated ? switchOn : switchOff).play();
      firstTime = false;
    }),
  );
};

applySwitchSFX(getNotifications);
applySwitchSFX(getGrid);

const Settings = () => {
  return (
    <Flex>
      <Flex
        row
        center
        left
        style={{ 'box-sizing': 'border-box', padding: '1em' }}
      >
        <span style={{ width: '12em' }}>🔔 Notif.</span>
        <Flex row center right>
          <Switch
            checked={getNotifications()}
            onChange={({ currentTarget }) => {
              setNotifications(currentTarget.checked);
            }}
          />
        </Flex>
      </Flex>
      <Flex
        row
        center
        left
        style={{ 'box-sizing': 'border-box', padding: '1em' }}
      >
        <span style={{ width: '12em' }}>📐 Grid</span>
        <Flex row center right>
          <Switch
            checked={getGrid()}
            onChange={({ currentTarget }) => {
              setGrid(currentTarget.checked);
            }}
          />
        </Flex>
      </Flex>
      <Flex
        row
        center
        left
        style={{ 'box-sizing': 'border-box', padding: '1em' }}
      >
        <span style={{ width: '12em' }}>🏠 Ownership</span>
        <Flex row center right>
          <Switch
            checked={getOwnership()}
            onChange={({ currentTarget }) => {
              setOwnership(currentTarget.checked);
            }}
          />
        </Flex>
      </Flex>
      <Flex
        row
        center
        left
        style={{ 'box-sizing': 'border-box', padding: '1em' }}
      >
        <span style={{ width: '12em' }}>🔊 Volume</span>
        <Flex row center right>
          <Slider
            defaultValue={getVolume()}
            min={0}
            max={100}
            onChange={({ currentTarget }) => {
              setVolume(currentTarget.valueAsNumber);
            }}
          />
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Settings;
