import createStorage, { createStoredSignal } from '@common/tools/storage';

export type Settings = {
  boardNotif: boolean;
  boardGrid: boolean;
  boardOwnership: boolean;
  boardSoundVolume: number;
};

const storage = createStorage<Settings>();

export const [getNotifications, setNotifications] = createStoredSignal(
  storage,
  'boardNotif',
  true,
);

export const [getGrid, setGrid] = createStoredSignal(
  storage,
  'boardGrid',
  true,
);

export const [getOwnership, setOwnership] = createStoredSignal(
  storage,
  'boardOwnership',
  true,
);

export const [getVolume, setVolume] = createStoredSignal(
  storage,
  'boardSoundVolume',
  50,
);
