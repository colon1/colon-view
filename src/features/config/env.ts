export const CACHE_API_ADDRESS =
  import.meta.env.VITE_CACHE_API_ADDRESS || `${window.location.origin}/cache`;
