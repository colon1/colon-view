import Flex from '@common/components/wrapper/Flex';

const Legals = () => (
  <Flex>
    <h3>Terms of Use</h3>
    <h4>1) Object</h4>
    <p>
      These Terms of Use deal with the access, the consultation and the use of
      the services offered by this Site.
    </p>
    <p>
      By accessing, using, consulting this Site or services offered by this
      Site, the User agrees fully, absolutely these Terms of Use. These Terms of
      Use may be edited, updated at any time, Users are invited to consult them
      regularly.
    </p>
    <h4>2) Cookies</h4>
    <p>
      Like third party vendors, we use{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://cookies.insites.com/"
      >
        Cookies
      </a>
      . The user can block or delete these Cookies at any time. However, note
      that the proper functioning of some services may require Cookies
      activation.
    </p>
    <h4>3) Data Privacy</h4>
    <p>The User agrees the following :</p>
    <p>
      By using our Site, servers gather basic information from your machine such
      as your{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/IP_address"
      >
        Internet protocol (IP) address
      </a>
      ,{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/Web_browser"
      >
        browser type
      </a>
      ,{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/Internet_service_provider"
      >
        internet service provider (ISP)
      </a>
      ,{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/Operating_system"
      >
        OS
      </a>
      ,{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/URL"
      >
        referring URL
      </a>
      .
    </p>
    <p>
      We care about your personal data, it is normal to inform you of what we do
      with it. Simply, collected information may be stored on your machine (via
      Cookies and / or local storage) and / or on our servers. We may use the
      collected information in order to improve, to personalize our Services, to
      do statistics, to make advertisements more relevant. Saved data is
      protected as far as possible. However, the hypothesis of a redemption or
      legal procedure would allow the transmission of such information.
    </p>
    <p>
      If you want to delete any information related to this Site on your machine
      then clear your{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/Web_cache"
      >
        cache
      </a>
      ,{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/Web_storage"
      >
        local storage
      </a>{' '}
      and Cookies. (<a href="https://www.refreshyourcache.com/en/home/">this</a>{' '}
      is a simple step by step guide)
    </p>
    <p>
      Our Privacy Policy may be updated at any time by posting the changes here.
    </p>
    <h4>4) Use of Services</h4>
    <p>
      This Site is NOT intended for people under 18 years. If your child is
      under 18, please allow them to use it only under your supervision.
    </p>
    <p>
      The User agrees to respect all the rules set forth herein in the use of
      the services of the Site editor. They agree, especially, not to create,
      send, publish, or allow the publication, broadcast, transmit, communicate
      or store, by any means whatsoever and whatever the recipient, illegal
      content or defamatory, abusive, discriminatory, derogatory, which may
      violate or infringe third parties rights or violate public order.
    </p>
    <p>
      The User agrees to respect the all Users' privacy and not to infringe the
      third parties rights. As such, they agree not to divulge or allow any
      information to be divulged in order to locate or identify individuals.
      Similarly, the User agrees not to broadcast private correspondence and to
      pay particular attention to the protection of minors, they agree not to
      create, broadcast, store, or let broadcast, pornographic or pedagogical
      content on this Site. They agree to respect intellectual property, and
      copyright and thus not to reproduce, represent, broadcast, modify, assign
      in any capacity whatsoever, content for which they have not obtained
      express authorization from the holders. The contents are understood as
      being any signs, signals, writings, images, sounds or messages of any
      kind.
    </p>
    <p>
      The User agrees not to infringe in any way whatsoever the intellectual
      property rights relating to this Site, as well as its contents and
      services. In particular, they agree not to reproduce, copy, distribute,
      communicate, assign or represent on any other site, as well as on any
      support having a commercial purpose, the elements and contents of this
      Site and its services.
    </p>
    <p>
      The User agrees not to hinder, disrupt, divert and more generally, not to
      act in any way, which would not be in accordance with the "normally
      expected" use of this Site. They agree not to use malicious{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://en.wikipedia.org/wiki/Internet_bot"
      >
        bots
      </a>
      , not to generate invasively or automatically content on this Site.
    </p>
    <p>
      The User is prohibited from broadcasting and / or prospecting for
      commercial purposes on this Site, they are prohibited to introduce,
      publish, send content that may be harmful to other Users.
    </p>
    <p>
      The editor of the Site is no way publisher of the content that is sent by
      Users. These contents are under the full responsibility of Users. The
      editor of the Site exercises moderation as much as possible.
    </p>
    <p>
      The User agrees that all sent content is accessible for an indefinite
      duration from this Site. See the Contact section to request a content
      deletion.
    </p>
    <p>
      The User grants to the editor of the Site a worldwide, non-exclusive,
      royalty-free, perpetual and irrevocable license to use, commercially or
      not, to post online and in any media present or future, to create
      derivative works, to allow downloading and / or the distribution of the
      contents they have produced on this Site. Any content that the User
      enters, sends, publishes or transmits may be used even after deleting it.
    </p>
    <p>The User accepts the specific conditions of the services they use.</p>
    <p>
      The User acknowledges the public nature, available to everyone, of the
      content sent and therefore refrains from distributing any personal data.
      Users are invited to report any element of illegal content. See the
      Contact section to report illegal content.
    </p>
    <h4>5) Liability</h4>
    <p>
      The editor of the Site sets up the means to ensure quality services.
      Nevertheless the editor of the Site cannot be held responsible for any
      damage, direct or indirect of any nature whatsoever, for any
      unavailability, failure, modification or error occurred when during the
      use of this Site or its services with the exception of those allegedly due
      to a breach of its obligations.
    </p>
    <p>
      The editor of the Site cannot guarantee continuity, accessibility and
      absolute security of the service, taking into account the risks related to
      the Internet. In the event a service interruption, all necessary actions
      to fix it will be taken as soon as possible.
    </p>
    <p>
      The User expressly acknowledges use of this Site and services at their
      sole and entire risk. The editor of the Site cannot be held responsible
      for relations (contractual or other) between advertisers / partners and
      Users of its sites and services unless the express contractual
      stipulation.
    </p>
    <p>
      This Site contains an amount of hypertext links to other sites, some of
      which may be communicated by the Users of this Site by means of pixels.
      However, the editor of the Site does not have the possibility to check the
      content of the visited site, and therefore assumes no liability for this
      fact.
    </p>
    {/* <Divider style={{ margin: "10px" }} /> */}
    <br />
    <h3>Host</h3>
    <h4>1) Name</h4>
    <p>The Site is hosted by DigitalOcean, LLC.</p>
    <h4>2) Contact</h4>
    <p>
      Contact DigitalOcean, LLC at{' '}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.digitalocean.com/company/contact/"
      >
        https://www.digitalocean.com/company/contact/
      </a>
    </p>
    <h4>3) Adresses</h4>
    <p>
      New York, NY <br /> 101 6th Ave
    </p>
    <p>
      Cambridge, MA <br /> 485 Massachusetts Ave
    </p>
    <p>
      Bangalore, India <br /> 43 Residency Rd
    </p>
  </Flex>
);

export default Legals;
