import { Component, createSignal, For } from 'solid-js';
import Flex from '@common/components/wrapper/Flex';
import { dice as diceSound } from '../audio';
import WelcomeModal from './WelcomeModal';
import createPlayer, { Player } from './createPlayer';
import PlayerComp from './Player';
import Dice from './dice/Dice';
import createRef from '@common/hooks/createRef';
import Victory from './Victory';
import BacteriaComp from './Bacteria';
import createBacterias from './createBacteriaSummoner';
import { SMALL_SCREEN } from './constants';

/**
 * Signal inoking the win screen, should contain the winner's name.
 */
export const [getWin, setWin] = createSignal<string | undefined>(undefined);

/**
 * Main board component, contain the image background, a player, a dice and bacterias.
 * @returns
 */
const Board: Component = () => {
  // image board reference
  const [getImageBoard, setImageBoard] = createRef<HTMLImageElement>();

  // player reactive signal
  const [getPlayer, setPlayer] = createSignal<Player>();

  // dice related signals
  const [getSide, setSide] = createSignal<number>(0);
  const [getThrowable, setThrowable] = createSignal<boolean>(true);

  // bacterias
  const getBacterias = createBacterias(getImageBoard, getPlayer);

  return (
    <Flex center>
      <div style={{ width: SMALL_SCREEN ? '400px' : '1000px' }}>
        {/* victory screen */}
        {getWin() ? <Victory /> : <></>}

        {/* welcome modal */}
        <WelcomeModal
          onCharacterType={(name, type) => setPlayer(createPlayer(name, type))}
        />

        {/* board */}
        <div style={{ position: 'absolute' }}>
          {/* image background */}
          <img
            ref={setImageBoard}
            src="/colon-view/assets/bitmaps/colon_alpha.png"
            width={SMALL_SCREEN ? '400px' : '1000px'}
          ></img>

          {/* player */}
          {getPlayer() ? <PlayerComp player={getPlayer()!} /> : <></>}

          {/* bacterias */}
          <For each={getBacterias()}>
            {(bacteria, index) => (
              <BacteriaComp data-index={index()} bacteria={bacteria} />
            )}
          </For>
        </div>

        {/* dice */}
        <Dice
          side={getSide()}
          disabled={!getThrowable()}
          onClick={() => {
            if (getThrowable()) {
              diceSound.play();
              const side = Math.floor(Math.random() * 6);
              setSide(side);
              setThrowable(false);

              const player = getPlayer();
              if (player) player.move(side + 1).then(() => setThrowable(true));
            }
          }}
        />
      </div>
    </Flex>
  );
};

export default Board;
