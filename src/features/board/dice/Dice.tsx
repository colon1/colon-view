import { Component, JSX } from 'solid-js';

const sideNames = ['front', 'top', 'left', 'right', 'bottom', 'back'];

export type DiceProps = {
  side: number;
  disabled: boolean;
} & JSX.HTMLAttributes<HTMLDivElement>;

/**
 * Dice component.
 * @param props dice properties
 * @returns dice component
 */
const Dice: Component<DiceProps> = (props) => (
  <div class={`dice__scene ${props.disabled ? 'disabled' : ''}`} {...props}>
    <div class={`dice__cube show-${sideNames[props.side]} show-same`}>
      <div class="dice__side dice__side--front"></div>
      <div class="dice__side dice__side--back"></div>
      <div class="dice__side dice__side--right"></div>
      <div class="dice__side dice__side--left"></div>
      <div class="dice__side dice__side--top"></div>
      <div class="dice__side dice__side--bottom"></div>
    </div>
  </div>
);

export default Dice;
