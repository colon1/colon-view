/**
 * A player can represent different kinds of food.
 */
export enum FoodType {
  Ananas,
  Apple,
  Banana,
  Berries,
  Carrot,
  Exotic,
  Lemon,
  Putri,
  Eroan,
  Jimin,
}

/**
 * Exported player's food types and their associated sprite paths.
 */
export const allExportedTypes: [FoodType, string][] = [
  [FoodType.Putri, '/colon-view/assets/bitmaps/fruit_putri.png'],
  [FoodType.Eroan, '/colon-view/assets/bitmaps/fruit_eroan.png'],
  [FoodType.Jimin, '/colon-view/assets/bitmaps/fruit_jimin.png'],
  [FoodType.Ananas, '/colon-view/assets/bitmaps/fruit_ananas.png'],
  [FoodType.Apple, '/colon-view/assets/bitmaps/fruit_apple.png'],
  [FoodType.Banana, '/colon-view/assets/bitmaps/fruit_banana.png'],
  [FoodType.Berries, '/colon-view/assets/bitmaps/fruit_berries.png'],
  [FoodType.Carrot, '/colon-view/assets/bitmaps/fruit_carrot.png'],
  [FoodType.Exotic, '/colon-view/assets/bitmaps/fruit_exotic.png'],
  [FoodType.Lemon, '/colon-view/assets/bitmaps/fruit_lemon.png'],
];
