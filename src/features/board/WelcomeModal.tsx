import { Component, createSignal, For } from 'solid-js';
import { createSlider } from 'solid-slider';
import Button from '@common/components/button/Button';
import Modal from '@common/components/wrapper/Modal';
import Flex from '@common/components/wrapper/Flex';
import { allExportedTypes, FoodType } from './foodType';

export type WelcomeModalProps = {
  onCharacterType: (name: string, type: FoodType) => void;
};

/**
 * Modal shown on game start. Asks for player's info.
 * @param props welcome modal properties
 * @returns welcome modal component
 */
const WelcomeModal: Component<WelcomeModalProps> = (props) => {
  // dialog
  const [isShowing, showDialog] = createSignal<boolean>(true);

  // slider
  const [slider, { current, next, prev }] = createSlider();

  // name
  const [getName, setName] = createSignal('');
  const [getNameErr, setNameErr] = createSignal<boolean>(false);
  slider;
  return (
    <Modal
      title={'Welcome to the Colon Game'}
      isShowing={isShowing()}
      hide={() => {
        console.warn('Please select your character');
      }}
    >
      <h4>🏃 Choose your character :</h4>
      <div
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        use:slider
        style={{
          height: '150px',
          color: 'text',
          display: 'flex',
          'align-items': 'center',
          'font-size': '55px',
        }}
      >
        <For each={allExportedTypes}>
          {([, src]) => (
            <Flex center>
              <img src={src} width={'80px'}></img>
            </Flex>
          )}
        </For>
      </div>
      <div style={{ 'text-align': 'center' }}>
        <Button color={'primary'} onClick={prev}>
          Prev
        </Button>
        <span style={{ 'margin-left': '4em', 'margin-right': '4em' }}>
          {current() + 1}
        </span>
        <Button color={'primary'} onClick={next}>
          Next
        </Button>
      </div>
      <br />
      <h4>✏️ What is your name :</h4>
      <span
        style={{ color: 'red', display: getNameErr() ? 'inherit' : 'none' }}
      >
        Please choose a name with 3-28 characters.
      </span>
      <input
        onChange={(e) => setName((e.target as HTMLInputElement).value)}
        type="text"
      ></input>
      <br />
      <Button
        color={'primary'}
        onClick={() => {
          const name = getName();
          if (name.length < 3 || name.length > 28) return setNameErr(true);
          props.onCharacterType(name, current());
          showDialog(false);
        }}
      >
        Validate
      </Button>
    </Modal>
  );
};

export default WelcomeModal;
