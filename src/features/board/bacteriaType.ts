/**
 * You can throw different bacteria types, for now only simple bacterias are used.
 */
export enum BacteriaType {
  Simple,
  Multiple,
}

/**
 * Exported bacteria types and their associated sprite path.
 */
export const allExportedTypes: [BacteriaType, string][] = [
  [BacteriaType.Simple, '/colon-view/assets/bitmaps/bac1.png'],
  [BacteriaType.Multiple, '/colon-view/assets/bitmaps/bac2.png'],
];
