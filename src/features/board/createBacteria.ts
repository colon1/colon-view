import createAnimationFrame from '@common/hooks/createAnimationFrame';
import { add, dot, Vector } from '@common/tools/geometry';
import { Accessor, createSignal } from 'solid-js';
import { BacteriaType } from './bacteriaType';

/**
 * A bacteria is tuple of bacteria type, position, and velocity.
 */
export type Bacteria = {
  type: BacteriaType;
  getPosition: Accessor<Vector<2>>;
  velocity: Vector<2>;
};

/**
 * Bacteria hook updating its position at each frame.
 * @param type bacteria type
 * @param position bacteria position
 * @param velocity bacteria velocity
 * @returns a bacteria
 */
const createBacteria = (
  type: BacteriaType,
  position: Vector<2>,
  velocity: Vector<2>,
): Bacteria => {
  const [getPosition, setPosition] = createSignal<Vector<2>>(position);

  // update bacteria position
  let lastFrame = Date.now();
  createAnimationFrame(() => {
    const now = Date.now();
    setPosition((position) =>
      add(position, dot(velocity, (now - lastFrame) * 1e-3)),
    );
    lastFrame = now;
  });

  const bacteria = {
    type,
    getPosition,
    velocity,
  };

  return bacteria;
};

export default createBacteria;
