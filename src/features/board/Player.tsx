import { Component, createMemo } from 'solid-js';
import clsx from 'classnames';
import { Player } from './createPlayer';
import { allExportedTypes } from './foodType';
import positions from './positions';
import animations from './Animations.module.scss';
import { SMALL_SCREEN } from './constants';

export type PlayerProps = { player: Player };

/**
 * Player component renderered on the HTML page.
 * @param props player properties
 * @returns player component
 */
const PlayerComp: Component<PlayerProps> = (props) => {
  // position on the board (in px)
  const getLeft = createMemo(() =>
    props.player.getStuck()
      ? (1300 * devicePixelRatio) / 1.5
      : positions[props.player.getPosition()][0] / 1.5,
  );
  const getTop = createMemo(() =>
    props.player.getStuck()
      ? (890 * devicePixelRatio) / 1.5
      : positions[props.player.getPosition()][1] / 1.5,
  );
  return (
    <div
      class={clsx({ [animations['shaking']]: props.player.getStuck() })}
      style={{
        position: 'absolute',
        left: `${getLeft()}px`,
        top: `${getTop()}px`,
        transition: 'all .25s',
      }}
    >
      <span
        class="text_with_2px_border"
        style={{
          color: 'white',
          'text-align': 'center',
          position: 'absolute',
          transform: 'translate(-50%, -40px)',
          'z-index': 1000,
          'font-size': '1.5em',
        }}
      >
        {props.player.name}
      </span>
      <img
        class={clsx(animations['giggle'])}
        style={{
          transform: 'translate(-50%, -50%)',
        }}
        width={SMALL_SCREEN ? '34px' : '85px'}
        src={
          props.player.getDigested()
            ? '/colon-view/assets/bitmaps/poop.png'
            : allExportedTypes[props.player.type][1]
        }
      ></img>
    </div>
  );
};

export default PlayerComp;
