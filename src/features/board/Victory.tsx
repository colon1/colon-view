import { Component, For } from 'solid-js';
import clsx from 'classnames';
import Flex from '@common/components/wrapper/Flex';
import { getWin } from './Board';

/**
 * Win screen.
 * @returns
 */
const Victory: Component = () => (
  <Flex
    center
    style={{
      position: 'absolute',
      width: '100vw',
      height: '100vh',
      top: 0,
      left: 0,
    }}
  >
    <span
      style={{
        color: 'white',
        'text-align': 'center',
        'font-weight': 'bold',
        'z-index': 99999,
        'font-size': '5em',
        top: '50%',
        left: '50%',
      }}
    >{`🎊 ${getWin()?.toUpperCase()} WINS! 🎊`}</span>
    <For each={Array.from({ length: 250 }, (_, i) => i)}>
      {(index) => <div class={clsx(`confetti-${index}`)}></div>}
    </For>
    <video
      style={{
        position: 'absolute',
        width: '100vw',
        height: '100vh',
        top: 0,
        left: 0,
        'z-index': 9999,
        opacity: 0.9,
      }}
      id="player"
      src="/colon-view/assets/videos/discopepe10.mp4"
      width={'100vw'}
      height={'100vh'}
      autoplay
      loop
    ></video>
  </Flex>
);

export default Victory;
