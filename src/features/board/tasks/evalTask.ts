import { Accessor, Setter } from 'solid-js';
import { playRandomPoop } from '../../audio';
import { Task } from './types';
import digestTask from './digestTask';
import moveTask from './moveTask';
import waitTask from './waitTask';
import winTask from './winTask';

const digestCases: Record<number, boolean> = {
  3: true,
  4: true,
  15: true,
  19: true,
  30: true,
  31: true,
  32: true,
};

const goBackwardsCases: Record<number, number> = {
  11: 6,
  24: 6,
  35: 4,
  36: 5,
  37: 12,
};

const goForwardsCases: Record<number, number> = {
  34: 6,
};

const goToStartCases: Record<number, boolean> = {
  25: true,
  43: true,
};

const winCase: Record<number, boolean> = {
  44: true,
};

/**
 * Evaluate the current case effect.
 * @param getPosition
 * @param setPosition
 * @param getDigested
 * @param setDigested
 * @param setStuck
 * @param name
 * @returns
 */
const evalTask =
  (
    getPosition: Accessor<number>,
    setPosition: Setter<number>,
    getDigested: Accessor<boolean>,
    setDigested: Setter<boolean>,
    setStuck: Setter<boolean>,
    name: string,
  ): Task =>
  async () => {
    const position = getPosition();
    if (digestCases[position])
      return [digestTask(setPosition, setDigested), waitTask(1000)];
    if (goBackwardsCases[position])
      return [
        moveTask(
          getPosition,
          setPosition,
          getDigested,
          setDigested,
          setStuck,
          name,
          goBackwardsCases[position],
          true,
        ),
        waitTask(1000),
      ];
    if (goForwardsCases[position])
      return [
        moveTask(
          getPosition,
          setPosition,
          getDigested,
          setDigested,
          setStuck,
          name,
          goForwardsCases[position],
          false,
        ),
        waitTask(1000),
      ];
    if (goToStartCases[position])
      return [
        async () => {
          playRandomPoop();
          setPosition(0);
          return [];
        },
        waitTask(1000),
      ];
    if (winCase[position]) return [winTask(setPosition, name), waitTask(1000)];
    return [];
  };

export default evalTask;
