import { Setter } from 'solid-js';
import { Task } from './types';
import { wait } from './waitTask';
import { screaming } from '../../audio';

/**
 * The player is stuck in the anus. They restart from the beginning.
 * @param setPosition
 * @param setStuck
 * @returns
 */
const stuckTask =
  (setPosition: Setter<number>, setStuck: Setter<boolean>): Task =>
  async () => {
    setStuck(true);
    screaming.play();
    await wait(10000);
    setStuck(false);
    setPosition(() => 0);
    return [];
  };

export default stuckTask;
