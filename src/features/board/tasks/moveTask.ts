import { Accessor, Setter } from 'solid-js';
import { Task } from './types';
import waitTask from './waitTask';
import walkTask from './walkTask';
import evalTask from './evalTask';
import stuckTask from './stuckTask';
import positions from '../positions';

/**
 * Move the player by `increment` cases (can be forwards, or backwards).
 * @param getPosition
 * @param setPosition
 * @param getDigested
 * @param setDigested
 * @param setStuck
 * @param name
 * @param increment
 * @param backwards
 * @returns
 */
const moveTask =
  (
    getPosition: Accessor<number>,
    setPosition: Setter<number>,
    getDigested: Accessor<boolean>,
    setDigested: Setter<boolean>,
    setStuck: Setter<boolean>,
    name: string,
    increment: number,
    backwards = false,
  ): Task =>
  async () => {
    const extra: Task[] = [waitTask(1000)];
    const position = getPosition();
    extra.push(
      evalTask(
        getPosition,
        setPosition,
        getDigested,
        setDigested,
        setStuck,
        name,
      ),
    );
    if (
      position + increment >= positions.length - 1 &&
      !getDigested() &&
      backwards === false
    ) {
      for (let i = 0; i < positions.length - 1 - position; i++) {
        extra.push(walkTask(setPosition, backwards));
        extra.push(waitTask(200));
      }
      extra.splice(1, 1, stuckTask(setPosition, setStuck));
      extra.splice(2, 1, waitTask(1500));
    } else
      for (let i = 0; i < increment; i++) {
        extra.push(walkTask(setPosition, backwards));
        extra.push(waitTask(i === increment - 1 ? 1000 : 200));
      }
    return extra;
  };

export default moveTask;
