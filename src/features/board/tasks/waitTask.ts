import { Task } from './types';

export const wait = (timeout: number) =>
  new Promise((resolve) => setTimeout(resolve, timeout));

/**
 * Wait `timeout` ms.
 * @param timeout time to wait in ms
 * @returns
 */
const waitTask =
  (timeout: number): Task =>
  async () => {
    await wait(timeout);
    return [];
  };

export default waitTask;
