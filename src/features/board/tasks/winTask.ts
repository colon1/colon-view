import { Setter } from 'solid-js';
import { setWin } from '../Board';
import { Task } from './types';
import { wait } from './waitTask';

/**
 * The player wins the game and restart from the beginning.
 * @param setPosition
 * @param name
 * @returns
 */
const winTask =
  (setPosition: Setter<number>, name: string): Task =>
  async () => {
    setWin(name);
    await wait(100000);
    setWin(undefined);
    setPosition(() => 0);
    return [];
  };

export default winTask;
