import { Setter } from 'solid-js';
import { playRandomPoop } from '../../audio';
import { Task } from './types';

/**
 * The player is digested, they may restart from the beginning if they're already disgested.
 * @param setPosition
 * @param setDigested
 * @returns
 */
const digestTask =
  (setPosition: Setter<number>, setDigested: Setter<boolean>): Task =>
  async () => {
    playRandomPoop();
    setDigested((digested) => {
      if (digested) setPosition(0);
      return !digested;
    });
    return [];
  };

export default digestTask;
