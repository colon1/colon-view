import { Setter } from 'solid-js';
import { playNextWalk } from '../../audio';
import positions from '../positions';
import { Task } from './types';

const length = positions.length;

/**
 * Move the player by 1 case (can be forwards, or backwards).
 * @param setPosition
 * @param backwards
 * @returns
 */
const walkTask =
  (setPosition: Setter<number>, backwards = false): Task =>
  async () => {
    playNextWalk();
    setPosition((position) =>
      Math.max(
        0,
        Math.min(length - 1, backwards ? position - 1 : position + 1),
      ),
    );
    return [];
  };

export default walkTask;
