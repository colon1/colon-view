import { Component } from 'solid-js';
import clsx from 'classnames';
import { Bacteria } from './createBacteria';
import { allExportedTypes } from './bacteriaType';
import animations from './Animations.module.scss';

export type BacteriaProps = { bacteria: Bacteria };

/**
 * Bacteria component renderered on the HTML page.
 * @param props bacteria properties
 * @returns bacteria component
 */
const BacteriaComp: Component<BacteriaProps> = (props) => (
  <div
    class={clsx(animations['rotate'])}
    style={{
      position: 'absolute',
      left: `${props.bacteria.getPosition()[0] / devicePixelRatio}px`,
      top: `${props.bacteria.getPosition()[1] / devicePixelRatio}px`,
      transition: 'all .25s',
    }}
  >
    <img
      style={{
        transform: 'translate(-50%, -50%)',
      }}
      width={'30px'}
      src={allExportedTypes[props.bacteria.type][1]}
    ></img>
  </div>
);

export default BacteriaComp;
