/**
 * Maximum quantity of bacterias a single player can summon.
 */
export const MAX_BACTERIAS_PER_PLAYER = 6;

export const SMALL_SCREEN = window.innerWidth <= 1080 * 1.1;
export const SMALL_COEF = 0.4;
