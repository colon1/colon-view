import { createSignal, Accessor } from 'solid-js';
import createControls from '@common/hooks/createControls';
import createOnDefined from '@common/hooks/createOnDefined';
import { dot, n2sqr, sub } from '@common/tools/geometry';
import createBacteria, { Bacteria } from './createBacteria';
import { BacteriaType } from './bacteriaType';
import positions from './positions';
import { MAX_BACTERIAS_PER_PLAYER } from './constants';
import { Player } from './createPlayer';

/**
 * Hook invoking & updating bacterias on the board.
 * @param getImageBoard image board accessor
 * @param getPlayer player accessor
 * @returns accessor of the updated array of bacterias
 */
const createBacteriaSummoner = (
  getImageBoard: Accessor<HTMLImageElement | undefined>,
  getPlayer: Accessor<Player | undefined>,
) => {
  // bacterias array
  const [getBacterias, setBacterias] = createSignal<Bacteria[]>([]);

  // call this once player & image board are defined
  createOnDefined<void, unknown>([getPlayer, getImageBoard], () => {
    const player = getPlayer()!;

    // create basic controls throwing bacterias on the board
    const controls = createControls(
      getImageBoard as Accessor<HTMLImageElement>,
    );
    controls.on('click', ({ offsetX, offsetY }) => {
      const playerPosition = dot(positions[player.getPosition()], 1);
      let direction = sub(playerPosition, [
        offsetX * devicePixelRatio,
        offsetY * devicePixelRatio,
      ]);
      direction = dot(
        direction,
        -100 / Math.max(Math.sqrt(n2sqr(direction)), 1e-3),
      );
      setBacterias((bacterias) =>
        [
          ...bacterias,
          createBacteria(BacteriaType.Simple, playerPosition, direction),
        ].slice(-MAX_BACTERIAS_PER_PLAYER),
      );
    });
  });

  return getBacterias;
};

export default createBacteriaSummoner;
