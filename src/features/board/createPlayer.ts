import { Accessor, createSignal } from 'solid-js';
import { FoodType } from './foodType';
import { Task } from './tasks/types';
import moveTask from './tasks/moveTask';

/**
 * A player can move, has a name, a type and can experience various states.
 */
export type Player = {
  name: string;
  type: FoodType;
  getDigested: Accessor<boolean>;
  getPosition: Accessor<number>;
  getStuck: Accessor<boolean>;
  move: (increment: number) => Promise<void>;
};

/**
 * Hook creating a player.
 * @param name player's name
 * @param type player's food type
 * @param digested player's digested state
 * @param position player's position on the board
 * @returns a player
 */
const createPlayer = (
  name: string,
  type: FoodType,
  digested = false,
  position = 0,
) => {
  // queue of tasks
  const queue: Task[] = [];

  // player's state signals
  const [getDigested, setDigested] = createSignal<boolean>(digested);
  const [getPosition, setPosition] = createSignal<number>(position);
  const [getStuck, setStuck] = createSignal<boolean>(false);

  /**
   * Move the player on the board, this may alter their state.
   * @param increment
   */
  const move = async (increment: number): Promise<void> => {
    queue.push(
      moveTask(
        getPosition,
        setPosition,
        getDigested,
        setDigested,
        setStuck,
        name,
        increment,
      ),
    );
    while (queue.length) {
      const task = queue.pop();
      if (task) {
        const extra = await task();
        queue.push(...extra);
      }
    }
  };

  return {
    name,
    type,
    getDigested,
    getPosition,
    getStuck,
    move,
  };
};

export default createPlayer;
