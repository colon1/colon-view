import { getVolume } from '../settings/store';
import { createEffect, on } from 'solid-js';

export const switchOn = new Audio('/colon-view/assets/sounds/switch-on.mp3');
export const switchOff = new Audio('/colon-view/assets/sounds/switch-off.mp3');
export const bip1 = new Audio('/colon-view/assets/sounds/bip1.mp3');
export const bip2 = new Audio('/colon-view/assets/sounds/bip2.mp3');
export const bip3 = new Audio('/colon-view/assets/sounds/bip3.mp3');
export const bip4 = new Audio('/colon-view/assets/sounds/bip4.mp3');
export const dice = new Audio('/colon-view/assets/sounds/dice.mp3');
export const poop1 = new Audio('/colon-view/assets/sounds/poop1.mp3');
export const poop2 = new Audio('/colon-view/assets/sounds/poop2.mp3');
export const poop3 = new Audio('/colon-view/assets/sounds/poop3.mp3');
export const poop4 = new Audio('/colon-view/assets/sounds/poop4.mp3');
export const poop5 = new Audio('/colon-view/assets/sounds/poop5.mp3');
export const poop6 = new Audio('/colon-view/assets/sounds/poop6.mp3');
export const poop7 = new Audio('/colon-view/assets/sounds/poop7.mp3');
export const walk1 = new Audio('/colon-view/assets/sounds/walk1.mp3');
export const walk2 = new Audio('/colon-view/assets/sounds/walk2.mp3');
export const screaming = new Audio('/colon-view/assets/sounds/screaming.mp3');

const poops = [poop1, poop2, poop3, poop4, poop5, poop6, poop7];
export const playRandomPoop = () =>
  poops[Math.floor(Math.random() * poops.length)].play();

const walks = [walk1, walk2];
let walkIndex = 0;
export const playNextWalk = () => walks[walkIndex++ % walks.length].play();

createEffect(
  on(getVolume, (volume) => {
    volume /= 100;
    switchOn.volume = volume;
    switchOff.volume = volume;
    bip1.volume = volume;
    bip2.volume = volume;
    bip3.volume = volume;
    bip4.volume = volume;
    dice.volume = volume;
    poop1.volume = volume;
    poop2.volume = volume;
    poop3.volume = volume;
    poop4.volume = volume;
    poop5.volume = volume;
    poop6.volume = volume;
    poop7.volume = volume;
    walk1.volume = volume;
    walk2.volume = volume;
    screaming.volume = volume;
  }),
);
