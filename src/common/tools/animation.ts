import { clone, equals, Vector } from './geometry';
import { Interpolation } from './interpolation';

export type AnimationInterval = {
  start: () => void;
  stop: () => void;
};

export const animate = (callback: () => void): AnimationInterval => {
  let isRunning = false;
  const loop = () => {
    callback();
    if (isRunning) requestAnimationFrame(loop);
  };
  const start = () => {
    if (isRunning) return;
    isRunning = true;
    loop();
  };
  const stop = () => {
    isRunning = false;
  };
  return {
    start,
    stop,
  };
};

/**
 * Smoothly adjust a value.
 * @param callback called when the value is updated
 * @param interpolation smooth interpolation applied
 * @param initialValue initial value
 * @param duration smooth animation duration
 * @returns smooth value setter
 */
export const smooth = (
  callback: (value: number) => void,
  interpolation: Interpolation<number>,
  initialValue: number,
  duration: number,
): ((to: number, from?: number) => void) => {
  let start = 0;
  let value = initialValue;
  let smoothFrom = initialValue;
  let smoothTo = initialValue;

  const interval = animate(() => {
    const t = (Date.now() - start) / (duration * 1000);
    if (t > 1) interval.stop();
    callback((value = interpolation(smoothFrom, smoothTo, t)));
  });

  const smoothValue = (to: number, from: number = value) => {
    if (smoothTo === to && from === value) return;
    smoothTo = to;
    smoothFrom = from;
    start = Date.now();
    interval.start();
  };

  return smoothValue;
};

/**
 * Smoothly adjust a vector.
 * @param callback called when the vector is updated
 * @param interpolation smooth interpolation applied
 * @param initialVector initial vector
 * @param duration smooth animation duration
 * @returns smooth vector setter
 */
export const smooth2 = (
  callback: (vector: Vector<2>) => void,
  interpolation: Interpolation<Vector<2>>,
  initialVector: Vector<2>,
  duration: number,
): ((to: Vector<2>, from?: Vector<2>) => void) => {
  let start = 0;
  let vector = clone(initialVector);
  let smoothFrom = clone(initialVector);
  let smoothTo = clone(initialVector);

  const interval = animate(() => {
    const t = (Date.now() - start) / (duration * 1000);
    if (t > 1) interval.stop();
    callback((vector = interpolation(smoothFrom, smoothTo, t)));
  });

  const smoothVector = (to: Vector<2>, from: Vector<2> = vector) => {
    if (equals(smoothTo, to) && equals(from, vector)) return;
    smoothTo = clone(to);
    smoothFrom = clone(from);
    start = Date.now();
    interval.start();
  };

  return smoothVector;
};
