import { createEffect, createSignal, on, Signal } from 'solid-js';

export type Storage<Items extends Record<string, unknown>> = {
  set: <ItemName extends string & keyof Items>(
    item: ItemName,
    value: Items[ItemName],
  ) => boolean;
  get: <ItemName extends string & keyof Items>(
    item: ItemName,
  ) => Items[ItemName] | null;
  canStore: () => boolean;
};

/**
 * Utility storage object.
 * @returns storage object
 */
const createStorage = <
  Items extends Record<string, unknown>,
>(): Storage<Items> => {
  /**
   * Check if the web browser support local storage.
   * @returns {boolean} true if the browser supports local storage, false if not
   */
  const canStore = (): boolean => Boolean(localStorage);

  if (!canStore())
    console.warn(
      "Your web browser doesn't support local storage. Your gaming experience may be degraded.",
    );

  const storage = {
    /**
     * Save an item in the browser local storage.
     * @param item item key
     * @param value item value
     * @returns {boolean} true if it's set, false if it's not
     */
    set: <ItemName extends string & keyof Items>(
      item: ItemName,
      value: Items[ItemName],
    ): boolean => {
      if (canStore()) {
        localStorage.setItem(item, JSON.stringify(value));
        return true;
      }
      return false;
    },

    /**
     * Retreive an item from the browser local storage.
     * @param item item key
     * @returns item value
     */
    get: <ItemName extends string & keyof Items>(
      item: ItemName,
    ): Items[ItemName] | null =>
      canStore() ? JSON.parse(localStorage.getItem(item) || 'null') : null,

    /**
     * Check if we can save and retreive data from the web browser local storage.
     * @returns {boolean}
     */
    canStore,
  };

  return storage;
};

/**
 * Create a signal persistent in the web browser local storage.
 * @param storage storage object
 * @param name name used by the web browser local storage
 * @param defaultValue default value used if no value if found in the web browser local storage
 * @returns persistent signal
 */
export const createStoredSignal = <
  Items extends Record<string, unknown>,
  ItemName extends string & keyof Items,
>(
  storage: Storage<Items>,
  name: ItemName,
  defaultValue: Items[ItemName],
): Signal<Items[ItemName]> => {
  const [getValue, setValue] = createSignal<Items[ItemName]>(defaultValue);
  createEffect(
    on(getValue, (newValue) => {
      storage.set(name, newValue);
    }),
  );
  const storedValue = storage.get(name);
  if (storedValue !== null) setValue(() => storedValue);
  return [getValue, setValue];
};

export default createStorage;
