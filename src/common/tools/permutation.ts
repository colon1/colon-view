/**
 * Compute next permutation.
 * @param array initial array
 * @returns next array permutation
 */
export const nextPermutation = (array: number[]): boolean => {
  // Find non-increasing suffix
  let i = array.length - 1;
  while (i > 0 && array[i - 1] >= array[i]) i--;
  if (i <= 0) return false;

  // Find successor to pivot
  let j = array.length - 1;
  while (array[j] <= array[i - 1]) j--;
  let temp = array[i - 1];
  array[i - 1] = array[j];
  array[j] = temp;

  // Reverse suffix
  j = array.length - 1;
  while (i < j) {
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
    i++;
    j--;
  }

  return true;
};

/**
 * Build (cache) permutations.
 * @param n array length
 */
export const buildPermutation = (
  n: number,
): {
  indexToPerm: (index: number) => number[] | undefined;
  permToIndex: (arr: number[]) => number | undefined;
} => {
  const indexToPerm: number[][] = [];
  const permToIndex = new Map<string, number>();

  const array: number[] = [];
  let index = 0;
  for (let i = 0; i < n; i++) array.push(i);
  indexToPerm.push(array.slice(0));
  permToIndex.set(JSON.stringify(array), index);

  while (nextPermutation(array)) {
    indexToPerm.push(array.slice(0));
    index++;
    permToIndex.set(JSON.stringify(array), index);
  }

  return {
    indexToPerm: (index: number) => indexToPerm[index],
    permToIndex: (arr: number[]) => permToIndex.get(JSON.stringify(arr)),
  };
};
