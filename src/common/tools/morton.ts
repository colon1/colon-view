import { add, Rectangle, Vector } from './geometry';

/**
 * Compute Morton code from 2D coordinates.
 * @param x 1st coordinate
 * @param y 2nd coordinate*
 * @returns Morton code
 */
const morton = (x: number, y: number): number => {
  const n = 32 - Math.clz32(Math.max(x, y));
  let res = 0;
  let mask = 1;
  for (let i = 0; i < n; i++) {
    res += ((x & mask) + 2 * (y & mask)) << i;
    mask = mask << 1;
  }
  return res;
};

const evenBits = (x: number): number => {
  const n = 32 - Math.clz32(x);
  let res = 0;
  let mask = 1;
  for (let i = 0; i <= n; i++) {
    res += x & mask ? 2 ** i : 0;
    mask = mask << 2;
  }
  return res;
};

/**
 * Compute 2D coordinates from Morton code.
 * @param m Morton code
 * @returns 2D coordinates
 */
export const mortonInv = (m: number): [number, number] => [
  evenBits(m),
  evenBits(m >> 1),
];

/**
 * Utility function calculating order for morton code `c`.
 * @param c morton code `c`
 * @returns
 */
export const computeOrder = (c: number) => {
  let order = 0;
  while (4 ** order <= c) order++;
  return order;
};

/**
 * Retreive the bounding box width containing all points before morton code `m` (excluded).
 * @param c morton code `c`
 */
export const mortonMaxX = (c: number, o = computeOrder(c)): number => {
  if (c === 0) return 0;
  const p = o - 1;
  switch (c >> (2 * p)) {
    case 0:
      return mortonMaxX(c & (4 ** p - 1), p);
    case 1:
      return 2 ** p + mortonMaxX(c & (4 ** p - 1), p);
    default:
      return 2 ** o;
  }
};

/**
 * Retreive the bounding box height containing all points before morton code `m` (excluded).
 * @param c morton code `c`
 */
export const mortonMaxY = (c: number, o = computeOrder(c)): number => {
  if (c === 0) return 0;
  const p = o - 1;
  switch (c >> (2 * p)) {
    case 0:
      return mortonMaxY(c & (4 ** p - 1), p);
    case 1:
      return 2 ** p;
    case 2:
      return 2 ** p + mortonMaxY(c & (4 ** p - 1), p);
    default:
      return 2 ** o;
  }
};

/**
 * Retreive the rectangles covering the curve until morton code `m` (excluded).
 * @param c morton code `c`
 */
export const getRectangles = (
  c: number,
  o = computeOrder(c),
  offset: Vector<2> = [0, 0],
): Rectangle<2>[] => {
  if (c === 0) return [];
  const p = o - 1;
  const unit = 2 ** p;
  switch (c >> (2 * p)) {
    case 0:
      return getRectangles(c & (4 ** p - 1), p, offset);
    case 1:
      return [
        [offset, add(offset, [unit, unit])],
        ...getRectangles(c & (4 ** p - 1), p, add(offset, [unit, 0])),
      ];
    case 2:
      return [
        [offset, add(offset, [2 * unit, unit])],
        ...getRectangles(c & (4 ** p - 1), p, add(offset, [0, unit])),
      ];
    case 3:
      return [
        [offset, add(offset, [2 * unit, unit])],
        [add(offset, [0, unit]), add(offset, [unit, 2 * unit])],
        ...getRectangles(c & (4 ** p - 1), p, add(offset, [unit, unit])),
      ];
    default:
      throw new Error('Cannot compute grid rectangles.');
  }
};

export default morton;
