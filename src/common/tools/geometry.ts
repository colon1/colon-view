export type Voxel<N extends number, V> = {
  index: Vector<N>;
  value: V;
};

export type Vector<N extends number, T = [number, ...Array<number>]> = T & {
  readonly length: N;
};

export type Rectangle<N extends number> = [Vector<N>, Vector<N>];

export const add = <N extends number>(u: Vector<N>, v: Vector<N>): Vector<N> =>
  u.map((c, i) => c + v[i]) as Vector<N>;

export const sub = <N extends number>(u: Vector<N>, v: Vector<N>): Vector<N> =>
  u.map((c, i) => c - v[i]) as Vector<N>;

export const dot = <N extends number>(u: Vector<N>, s: number): Vector<N> =>
  u.map((c) => s * c) as Vector<N>;

export const mod = <N extends number>(u: Vector<N>, s: number): Vector<N> =>
  (u = u.map((x) => ((x % s) + s) % s) as Vector<N>);

export const apply = <N extends number>(
  u: Vector<N>,
  f: (value: number, index: number, array: number[]) => number,
): Vector<N> => (u = u.map(f) as Vector<N>);

export const hadam = <N extends number>(
  u: Vector<N>,
  v: Vector<N>,
): Vector<N> => u.map((c, i) => c * v[i]) as Vector<N>;

export const clone = <N extends number>(u: Vector<N>): Vector<N> =>
  [...u] as Vector<N>;

export const equals = <N extends number>(
  u: Vector<N>,
  v: Vector<N>,
): boolean => {
  for (let i = 0; i < u.length; i++) if (u[i] !== v[i]) return false;
  return true;
};

export const mean = <N extends number>(u: Vector<N>, v: Vector<N>): Vector<N> =>
  u.map((c, i) => (c + v[i]) / 2) as Vector<N>;

export const fixRect = <N extends number>([u, v]: Rectangle<N>): Rectangle<N> =>
  [
    u.map((c, i) => (c > v[i] ? (u[i] + v[i]) / 2 : c)),
    v.map((c, i) => (c < u[i] ? (u[i] + v[i]) / 2 : c)),
  ] as Rectangle<N>;

/**
 * Returns vector squared norm2.
 * @param u vector
 * @returns u squared norm2
 */
export const n2sqr = <N extends number>(u: Vector<N>): number =>
  u.reduce((acc, c) => acc + c * c, 0);
