/**
 * Ensure that a value is well defined.
 * @param value
 * @returns
 */
const ensure = <T>(value: T | undefined | null) => {
  if (value === undefined || value === null)
    throw new Error('Value is not specified.');
  return value;
};

export default ensure;
