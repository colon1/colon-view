export const groupByMap = <T>(arr: T[], keyOf: (o: T) => string) => {
  const groups = new Map<string, T[]>();
  arr.forEach((o) => {
    const key = keyOf(o);
    const group = groups.get(key);
    if (!group) groups.set(key, [o]);
    else group.push(o);
  });
  return groups;
};

export const groupBy = <T>(arr: T[], keyOf: (o: T) => string) =>
  Array.from(groupByMap(arr, keyOf));

export const clamp = (min: number, x: number, max: number) =>
  x < min ? min : x > max ? max : x;
