/**
 * Request notification permission.
 * @returns promised notification permission
 */
export const requestPermission = (): Promise<NotificationPermission> =>
  new Promise((resolve) => {
    if (Notification.permission === 'default')
      Notification.requestPermission(resolve);
    else resolve(Notification.permission);
  });

/**
 * Spawn a notification if authorized to do so.
 * @param title
 * @param icon
 * @param body
 * @returns
 */
export const spawnNotification = (
  title: string,
  icon: string,
  body: string,
): Promise<NotificationPermission> => {
  // Let's check if the browser supports notifications
  if (!('Notification' in window)) return Promise.resolve('denied');

  // Create the notification
  const notif = () => {
    const notification = new Notification(title, {
      icon,
      body,
    });
    notification.onclick = function onClick() {
      window.focus();
      this.close();
    };
  };

  return requestPermission().then((permission) => {
    // If the user accepts, let's create a notification
    if (permission === 'granted') notif();
    return permission;
  });
};
