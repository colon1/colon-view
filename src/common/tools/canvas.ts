import { Vector, Voxel } from './geometry';

/**
 * Resize canvas to fit window dimensions.
 * @param canvas HTML canvas element
 * @returns
 */
export const resizeCanvas = (
  canvas: HTMLCanvasElement,
  tolerance = 0.1,
): boolean => {
  const width = Math.round(window.innerWidth * devicePixelRatio);
  const height = Math.round(window.innerHeight * devicePixelRatio);
  if (
    Math.abs(canvas.width - width) > tolerance ||
    Math.abs(canvas.height - height) > tolerance
  ) {
    canvas.width = width;
    canvas.height = height;
    return true;
  }
  return false;
};

export type RGBColor = { r: number; g: number; b: number };
export type RGBAColor = RGBColor & { a: number };
export type RGBPixel = Voxel<2, RGBColor>;
export type RGBAPixel = Voxel<2, RGBAColor>;

/**
 * Check if number[] is [number, number, number].
 * @param test number array
 * @returns {boolean} is in RGB format ?
 */
const isRgbArray = (test: number[]): test is [number, number, number] =>
  test.length === 3;

/**
 * Converts rgb to hex color. (https://stackoverflow.com/a/5624139)
 * @param {RGBColor} color rgb color
 * @returns {string} hex color
 */
export const rgbToHex = ({ r, g, b }: RGBColor): string =>
  // eslint-disable-next-line no-bitwise
  `#${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)}`;

/**
 * Converts hex to rgb color. (https://stackoverflow.com/a/5624139)
 * @param {string} hex hex color
 * @returns {RGBColor} rgb color
 */
export const hexToRgb = (hex: string): RGBColor => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i
    .exec(hex)
    ?.slice(1)
    .map((res) => parseInt(res, 16));
  if (!result || !isRgbArray(result))
    throw new Error(`Cannot convert color ${hex} to rbg format.`);
  return { r: result[0], g: result[1], b: result[2] };
};

export const setRGB = (
  data: Uint8ClampedArray,
  index: number,
  { r, g, b }: RGBColor,
): void => {
  data[index + 0] = r;
  data[index + 1] = g;
  data[index + 2] = b;
  data[index + 3] = 255;
};

export const setRGBA = (
  data: Uint8ClampedArray,
  index: number,
  { r, g, b, a }: RGBAColor,
): void => {
  data[index + 0] = r;
  data[index + 1] = g;
  data[index + 2] = b;
  data[index + 3] = a;
};

export const getRGB = (data: Uint8ClampedArray, index: number): RGBColor => ({
  r: data[index + 0],
  g: data[index + 1],
  b: data[index + 2],
});

export const getRGBA = (data: Uint8ClampedArray, index: number): RGBAColor => ({
  r: data[index + 0],
  g: data[index + 1],
  b: data[index + 2],
  a: data[index + 3],
});

/**
 * Create an HTML canvas element.
 * @param width canvas width (in px)
 * @param height canvas height (in px)
 * @returns HTML canvas element
 */
export const createCanvas = (
  width: number,
  height: number,
): HTMLCanvasElement => {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  return canvas;
};

/**
 * Get canvas context 2D.
 * @param canvas HTML canvas element
 * @returns canvas context 2D
 */
export const getCanvasContext2D = (
  canvas: HTMLCanvasElement,
): CanvasRenderingContext2D => {
  const context = canvas.getContext('2d');
  if (context === null) throw new Error('No context found on that canvas.');
  return context;
};

/**
 * Get pixel colors.
 * @param canvas HTML canvas element
 * @param indexes pixel positions in chunk space
 * @returns {RGBPixel} RGB pixels
 */
export const getCanvasPixels = (
  canvas: HTMLCanvasElement,
  indexes: Vector<2>[],
): RGBPixel[] => {
  const ctx = canvas.getContext('2d');
  if (ctx === null) throw new Error('No context found on that canvas.');
  const { width, height } = canvas;
  const imageData = ctx.getImageData(0, 0, width, height);
  return indexes.map(([x, y]) => ({
    index: [x, y],
    value: getRGB(imageData.data, (width * y + x) * 4),
  }));
};

/**
 * Set pixel colors.
 * @param canvas HTML canvas element
 * @param pixels RGB pixels to set in chunk space
 */
export const setCanvasPixels = (
  canvas: HTMLCanvasElement,
  pixels: RGBPixel[],
): void => {
  const ctx = canvas.getContext('2d');
  if (ctx === null) throw new Error('No context found on that canvas.');
  const { width, height } = canvas;
  const imageData = ctx.getImageData(0, 0, width, height);
  pixels.forEach(({ index, value }) => {
    const [x, y] = index as Vector<2>;
    setRGB(imageData.data, (width * y + x) * 4, value);
  });
  ctx.putImageData(imageData, 0, 0);
};

/**
 * Retreive pixel data buffer from an image.
 * @param img
 * @returns pixel data buffer
 */
export const getImagePixelData = (img: HTMLImageElement): ImageData => {
  const canvas = document.createElement('canvas');
  canvas.width = img.width;
  canvas.height = img.height;
  const context = getCanvasContext2D(canvas);
  context.drawImage(img, 0, 0, img.width, img.height);
  return context.getImageData(0, 0, img.width, img.height);
};

/**
 * Retreive image from a pixel data buffer.
 * @param imgData
 * @returns image
 */
export const getImageElement = (imgData: ImageData) => {
  const canvas = document.createElement('canvas');
  const ctx = getCanvasContext2D(canvas);
  canvas.width = imgData.width;
  canvas.height = imgData.height;
  ctx.putImageData(imgData, 0, 0);

  return canvas;
};
