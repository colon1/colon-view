import { add, dot, sub, Vector } from './geometry';

export type Interpolation<T> = (from: T, to: T, t: number) => T;

export const lerp: Interpolation<number> = (from, to, t) =>
  t < 0 ? from : t > 1 ? to : from + t * (to - from);

export const lerp2: Interpolation<Vector<2>> = (from, to, t) =>
  t < 0 ? from : t > 1 ? to : add(from, dot(sub(to, from), t));

export const inPow =
  (p: number): Interpolation<number> =>
  (from, to, t) =>
    t < 0 ? from : t > 1 ? to : from + t ** p * (to - from);

export const inPow2 = (p: number): Interpolation<Vector<2>> => {
  const pow = inPow(p);
  return (from, to, t) => lerp2(from, to, pow(0, 1, t));
};

export const outPow =
  (p: number): Interpolation<number> =>
  (from, to, t) =>
    t < 0 ? from : t > 1 ? to : from + (1 - (1 - t) ** p) * (to - from);

export const outPow2 = (p: number): Interpolation<Vector<2>> => {
  const pow = outPow(p);
  return (from, to, t) => lerp2(from, to, pow(0, 1, t));
};
