export type Action = {
  mockAction: (
    key: string,
    timeout: number,
    revert: () => void,
  ) => Map<string, NodeJS.Timeout>;
  validateAction: (key: string) => void;
  clear: () => void;
};

const action = (): Action => {
  const map = new Map<string, NodeJS.Timeout>();

  /**
   * Mock action, revert it if it's not validated in time.
   * @param key action key
   * @param timeout time to wait in ms before reverting the action
   * @param revert function reverting the action
   */
  const mockAction = (key: string, timeout: number, revert: () => void) =>
    map.set(
      key,
      setTimeout(() => {
        revert();
        map.delete(key);
      }, timeout),
    );
  /**
   * Validate the action, don't revert it.
   * @param key action key
   */
  const validateAction = (key: string) => {
    const timeout = map.get(key);
    if (timeout) clearTimeout(timeout);
    map.delete(key);
  };

  /**
   * Clear all the timeouts.
   */
  const clear = () => {
    Array.from(map.values()).forEach((timeout) => clearTimeout(timeout));
    map.clear();
  };

  return { mockAction, validateAction, clear };
};

export default action;
