export type Limiter = {
  limit: () => number;
  getScore: () => number;
  setScore: (newScore: number) => void;
};

/**
 * Simple rate limiter.
 * @param {number} [rate] the expected validated queries per second (long term)
 * @param {number} [tolerance] simultaneous valid queries tolerance (short term)
 * @param {number} [delay] minimum delay between two valid queries (in seconds)
 * @param {number} [score] initial score (score = tolernace means limit is true at the start)
 * @return {Limiter} callback passing remaining time to wait (+ score getter & setter).
 */
const limiter = (rate = 1, tolerance = 1, delay = 0, score = 0): Limiter => {
  let date = Date.now();
  const limit = () => {
    const tempDate = Date.now();
    const dt = (tempDate - date) * 1e-3;
    const tempScore = Math.max(0, score - dt * rate);
    const dtr = Math.max((tempScore - tolerance + 1) / rate, delay - dt, 0);
    if (dtr > 0) return dtr;
    date = tempDate;
    score = tempScore + 1;
    return 0;
  };
  const getScore = () => Math.max(0, score - (Date.now() - date) * 1e-3 * rate);
  const setScore = (newScore: number) => (score = newScore);
  return { limit, getScore, setScore };
};

export default limiter;
