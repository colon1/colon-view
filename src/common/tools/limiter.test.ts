// TODO: add jest and configure it

// import limiter from "./limiter";

// const wait = (duration: number) =>
//   new Promise((resolve) => setTimeout(resolve, duration));

// const speed = 1;
// const EPS = 1e-3;

// describe("Limiter", () => {
//   it("should respect the rate limit", async () => {
//     const { limit } = limiter(speed, 1, 0);
//     limit();
//     await wait(400 / speed);
//     expect(limit()).toBeGreaterThan(EPS);
//     await wait(400 / speed);
//     expect(limit()).toBeGreaterThan(EPS);
//     await wait(400 / speed);
//     expect(limit()).toBeLessThan(EPS);
//   });
//   it("should respect the tolerance", async () => {
//     const { limit } = limiter(speed, 5, 0);
//     limit();
//     limit();
//     limit();
//     limit();
//     limit();
//     await wait(400 / speed);
//     expect(limit()).toBeGreaterThan(EPS);
//     await wait(400 / speed);
//     expect(limit()).toBeGreaterThan(EPS);
//     await wait(400 / speed);
//     expect(limit()).toBeLessThan(EPS);
//   });
//   it("should respect the delay", async () => {
//     const { limit } = limiter(100 * speed, 100, 0.5 / speed);
//     limit();
//     await wait(400 / speed);
//     expect(limit()).toBeGreaterThan(EPS);
//     await wait(600 / speed);
//     expect(limit()).toBeLessThan(EPS);
//   });
// });
