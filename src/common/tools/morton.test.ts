// TODO: add jest and configure it

// import morton from "./morton";

// describe("Morton Code", () => {
//   const model = [
//     [0, 0],
//     [1, 0],
//     [0, 1],
//     [1, 1],
//     [2, 0],
//     [3, 0],
//     [2, 1],
//     [3, 1],
//     [0, 2],
//     [1, 2],
//     [0, 3],
//     [1, 3],
//     [2, 2],
//     [3, 2],
//     [2, 3],
//     [3, 3],
//   ];

//   it("should guess the correct sequence for n = 2", () => {
//     const inputSequence = model;
//     const expectedSequence = new Array(16).fill(null).map((_, i) => i);
//     expect(inputSequence.map(([x, y]) => morton(2, x, y, false))).toEqual(
//       expectedSequence
//     );
//   });

//   it("should guess the correct SIGNED sequence for n = 2", () => {
//     const inputSequence = model.map(([x, y]) => [x - 2, y - 2]);
//     const expectedSequence = new Array(16).fill(null).map((_, i) => i);
//     expect(inputSequence.map(([x, y]) => morton(2, x, y, true))).toEqual(
//       expectedSequence
//     );
//   });

//   it("should guess the correct sequence for n = 4", () => {
//     const inputSequence = [
//       ...model,
//       ...model.map(([x, y]) => [x + 4, y]),
//       ...model.map(([x, y]) => [x, y + 4]),
//       ...model.map(([x, y]) => [x + 4, y + 4]),
//     ];
//     const expectedSequence = new Array(64).fill(null).map((_, i) => i);
//     expect(inputSequence.map(([x, y]) => morton(4, x, y, false))).toEqual(
//       expectedSequence
//     );
//   });

//   it("should guess the correct SIGNED sequence for n = 4", () => {
//     const inputSequence = [
//       ...model.map(([x, y]) => [x - 4, y - 4]),
//       ...model.map(([x, y]) => [x, y - 4]),
//       ...model.map(([x, y]) => [x - 4, y]),
//       ...model,
//     ];
//     const expectedSequence = new Array(64).fill(null).map((_, i) => i);
//     expect(inputSequence.map(([x, y]) => morton(4, x, y, true))).toEqual(
//       expectedSequence
//     );
//   });
// });
