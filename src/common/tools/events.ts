import mitt, {
  EventHandlerMap,
  EventType,
  Handler,
  WildcardHandler,
  Emitter as MittEmitter,
} from 'mitt';

export type SimpleEmitter<Events extends Record<EventType, unknown>> = {
  on: {
    <Key extends keyof Events>(type: Key, handler: Handler<Events[Key]>): void;
    (type: '*', handler: WildcardHandler<Events>): void;
  };
  off: {
    <Key extends keyof Events>(
      type: Key,
      handler?: Handler<Events[Key]> | undefined,
    ): void;
    (type: '*', handler: WildcardHandler<Events>): void;
  };
  clear: () => void;
};

export type Emitter<Events extends Record<EventType, unknown>> =
  MittEmitter<Events> & { clear: () => void };

/**
 * https://github.com/developit/mitt/issues/57#issuecomment-304261385
 * @returns event emitter
 */
const emitter = <Events extends Record<EventType, unknown>>(
  all?: EventHandlerMap<Events>,
): Emitter<Events> => {
  const map = all || new Map();
  const events = mitt<Events>(map);
  return { ...events, clear: () => map.clear() };
};

export default emitter;
