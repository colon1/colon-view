import { Component, JSX } from 'solid-js';

export type EllipsisProps = JSX.HTMLAttributes<HTMLDivElement> & {
  dotColor: string;
};

const Ellipsis: Component<EllipsisProps> = (props) => (
  <div class="lds-ellipsis" {...props}>
    <div style={{ 'background-color': props.dotColor }}></div>
    <div style={{ 'background-color': props.dotColor }}></div>
    <div style={{ 'background-color': props.dotColor }}></div>
    <div style={{ 'background-color': props.dotColor }}></div>
  </div>
);

export default Ellipsis;
