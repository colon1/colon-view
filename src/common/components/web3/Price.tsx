import { BigNumber } from 'ethers';
import { Component } from 'solid-js';
import Flex from '@common/components/wrapper/Flex';

export type PriceProps = { price: BigNumber };

const Price: Component<PriceProps> = (props) => (
  <Flex row center>
    <img
      src="assets/icons/ethereum.png"
      width={'1.25em'}
      height={'1.25em'}
      style={{
        width: '1.25em',
        height: '1.25em',
        'margin-right': '0.5em',
      }}
    ></img>
    {(props.price.div(1e15).toNumber() * 1e-3).toFixed(3)}{' '}
  </Flex>
);

export default Price;
