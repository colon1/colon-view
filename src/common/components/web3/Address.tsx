import { Component, createSignal } from 'solid-js';
import { followCursor } from 'tippy.js';
import { useTippy } from 'solid-tippy';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/dist/svg-arrow.css';
import 'tippy.js/animations/shift-away.css';
import clsx from 'classnames';
import QRCode from './QRCode';
import styles from '@common/components/common.module.scss';

export type AddressProps = {
  link: string;
  qrcode: string;
  address: string;
};

const Address: Component<AddressProps> = (props) => {
  const [getAnchor, setAnchor] = createSignal<HTMLAnchorElement>();

  useTippy(getAnchor, {
    props: {
      theme: 'vanilla',
      content: (
        <QRCode data={props.qrcode} width="128px" height="128px"></QRCode>
      ) as Element,
      interactive: false,
      arrow: true,
      followCursor: 'horizontal',
      animation: 'shift-away',
      trigger: 'mouseenter',
      plugins: [followCursor],
    },
    hidden: true,
  });

  return (
    <a
      ref={setAnchor}
      href={props.link}
      target="_blank"
      class={clsx(styles['long-text'])}
    >
      {props.address}
    </a>
  );
};

export default Address;
