import { Component, createEffect, createSignal, JSX } from 'solid-js';
import qr from 'qrcode';

export type QRCodeProps = {
  data: string;
} & JSX.ImgHTMLAttributes<HTMLImageElement>;

const QRCode: Component<QRCodeProps> = (props) => {
  const [getURL, setURL] = createSignal<string>();

  createEffect(() => {
    qr.toDataURL(props.data)
      .then((url) => {
        setURL(url);
      })
      .catch((err) => {
        console.error(err);
      });
  });

  return (
    <img src={getURL()} {...props}>
      {props.children}
    </img>
  );
};

export default QRCode;
