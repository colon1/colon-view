import { splitProps } from 'solid-js';
import { JSX } from 'solid-js/jsx-runtime';
import clsx from 'classnames';
import styles from './Button.module.scss';

export type WhiteButtonProps = {
  color: 'primary' | 'secondary' | 'paper';
} & JSX.ButtonHTMLAttributes<HTMLButtonElement>;

const Button = (props: WhiteButtonProps) => {
  const [local, otherProps] = splitProps(props, ['color']);
  return (
    <button {...otherProps} class={clsx(styles[local.color], otherProps.class)}>
      {otherProps.children}
    </button>
  );
};

export default Button;
