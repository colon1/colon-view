import { splitProps } from 'solid-js';
import { JSX } from 'solid-js/jsx-runtime';
import clsx from 'classnames';
import commonStyles from '../common.module.scss';
import iconButtonStyles from './IconButton.module.scss';

type IconButtonProps = {
  src?: string;
  alt?: string;
} & JSX.ButtonHTMLAttributes<HTMLButtonElement>;

const IconButton = (props: IconButtonProps) => {
  const [local, otherProps] = splitProps(props, ['src', 'alt']);
  return (
    <button
      {...otherProps}
      class={clsx(
        commonStyles.background,
        commonStyles['content-box'],
        commonStyles['padding'],
        iconButtonStyles['icon-button'],
        otherProps.class,
      )}
    >
      <img
        class={commonStyles['scale-hover']}
        src={local.src}
        alt={`${local.alt}-icon`}
        width="2.5em"
        height="2.5em"
      />
    </button>
  );
};

export default IconButton;
