import { Component, JSX, splitProps } from 'solid-js';
import clsx from 'classnames';
import styles from './Flex.module.scss';

type FlexProps = {
  row?: boolean;
  reverse?: boolean;
  width?: number | string;
  height?: number | string;
  center?: boolean;
  right?: boolean;
  left?: boolean;
  top?: boolean;
  bottom?: boolean;
  style?: JSX.CSSProperties;
} & JSX.HTMLAttributes<HTMLDivElement>;

const Flex: Component<FlexProps> = (props) => {
  const [local, otherProps] = splitProps(props, [
    'row',
    'reverse',
    'width',
    'height',
    'center',
    'right',
    'left',
    'top',
    'bottom',
  ]);
  return (
    <div
      {...otherProps}
      class={clsx({
        [styles.flex]: true,
        [styles.row]: local.row,
        [styles.column]: !local.row,
        [styles.reverse]: local.reverse,
        [styles.center]: local.center,
        [styles.right]: local.right,
        [styles.left]: local.left,
        [styles.top]: local.top,
        [styles.bottom]: local.bottom,
        ...(otherProps.class ? { [otherProps.class]: true } : {}),
      })}
      style={{
        ...(local.width ? { 'max-width': local.width } : {}),
        ...(local.height ? { 'max-height': local.height } : {}),
        ...(otherProps.style as JSX.CSSProperties),
      }}
    >
      {otherProps.children}
    </div>
  );
};

export default Flex;
