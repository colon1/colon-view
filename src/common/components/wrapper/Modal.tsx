import { Component, splitProps } from 'solid-js';
import { Portal } from 'solid-js/web';
import { JSX } from 'solid-js/jsx-runtime';
import clsx from 'classnames';
import createRef from '@common/hooks/createRef';
import createClickListener from '../../hooks/createClickListener';
import styles from '../common.module.scss';
import modalStyles from './Modal.module.scss';

export type ModalProps = {
  isShowing: boolean;
  hide: () => void;
  title: string;
} & JSX.HTMLAttributes<HTMLDivElement>;

const Modal: Component<ModalProps> = (props) => {
  const [local, otherProps] = splitProps(props, ['isShowing', 'hide', 'title']);
  const [getRef, setRef] = createRef<HTMLDivElement>();
  createClickListener(getRef, () => local.hide());
  return (
    <>
      {local.isShowing ? (
        <Portal>
          <div class={clsx(styles.paper)}>
            <div class={clsx(modalStyles['modal-overlay'])}>
              <div class={clsx(modalStyles['modal-wrapper'])} ref={setRef}>
                <div class={clsx(modalStyles.modal, otherProps.class)}>
                  <div class={clsx(modalStyles['modal-header'])}>
                    <h2>{local.title}</h2>
                    <button
                      type="button"
                      class={clsx(modalStyles['modal-close-button'])}
                      onClick={local.hide}
                    >
                      <span>&times;</span>
                    </button>
                  </div>
                  {otherProps.children}
                </div>
              </div>
            </div>
          </div>
          ,
        </Portal>
      ) : (
        <></>
      )}
    </>
  );
};

export type ModalBodyProps = JSX.HTMLAttributes<HTMLDivElement>;

export const ModalBody = (props: ModalBodyProps) => (
  <div class={clsx(modalStyles['modal-body'])} {...props}>
    {props.children}
  </div>
);

export default Modal;
