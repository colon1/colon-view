import { Component, splitProps } from 'solid-js';
import { JSX } from 'solid-js/jsx-runtime';
import styles from './Ratio.module.scss';

export type RatioProps = JSX.HTMLAttributes<HTMLDivElement> & {
  style?: JSX.CSSProperties;
  ratio: number;
};

const Ratio: Component<RatioProps> = (props) => {
  const [local, otherProps] = splitProps(props, ['ratio', 'style']);
  return (
    <div
      {...otherProps}
      class={styles.ratio}
      style={{
        paddingBottom: `${100 / local.ratio}%`,
        ...(local.style as JSX.CSSProperties | undefined),
      }}
    >
      <div>{otherProps.children}</div>
    </div>
  );
};

export default Ratio;
