import { createMemo, createSignal, JSX, splitProps } from 'solid-js';

const calculateBackgroundImage = (min: number, max: number, value: number) => {
  const valPercent = value - min / (max - min);
  // TODO: import SCSS variable instead of hardcoding the primary color
  return `-webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${valPercent}%, #de0095), color-stop(${valPercent}%, #c7c7d1))`;
};

export type InputRangeProps = JSX.InputHTMLAttributes<HTMLInputElement> & {
  defaultValue: number;
  min: number;
  max: number;
  style?: JSX.CSSProperties;
  onInput?: JSX.EventHandler<HTMLInputElement, InputEvent>;
};

const Slider = (props: InputRangeProps) => {
  const [local, otherProps] = splitProps(props, [
    'defaultValue',
    'min',
    'max',
    'style',
    'onInput',
  ]);
  const [getValue, setValue] = createSignal<number>(local.defaultValue);
  const getStyle = createMemo<JSX.CSSProperties>(() => ({
    ...(local.style as JSX.CSSProperties),
    ['background-image']: calculateBackgroundImage(
      local.min,
      local.max,
      getValue(),
    ),
  }));

  return (
    <input
      {...otherProps}
      type="range"
      value={local.defaultValue}
      min={local.min}
      max={local.max}
      style={getStyle()}
      onInput={(event) => {
        if (local.onInput) local.onInput(event);
        setValue(event.currentTarget.valueAsNumber);
      }}
    ></input>
  );
};

export default Slider;
