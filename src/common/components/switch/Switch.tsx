import { JSX, splitProps } from 'solid-js';
import clsx from 'classnames';
import styles from './Switch.module.scss';

export type SwitchProps = JSX.InputHTMLAttributes<HTMLInputElement> & {
  checked?: boolean;
};

const Switch = (props: SwitchProps) => {
  const [local, otherProps] = splitProps(props, ['checked']);
  return (
    <label class={clsx(styles.switch)}>
      <input {...otherProps} type="checkbox" checked={local.checked} />
      <span class={clsx(styles.slider, styles.round)}></span>
    </label>
  );
};

export default Switch;
