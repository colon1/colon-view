import styles from './DropdownItem.module.scss';

export type DropdownItemProps = {
  title: string;
  src: string;
  onClick: () => void;
};

const DropdownItem = (props: DropdownItemProps) => (
  <button onClick={() => props.onClick()}>
    <img
      class={styles['dropdown-item']}
      src={props.src}
      alt={`dropdown-item-${props.title.toLowerCase().replace(/\s/g, '-')}`}
      width="1.5em"
      height="1.5em"
    ></img>
    <span>{props.title}</span>
  </button>
);

export default DropdownItem;
