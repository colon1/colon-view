import { createSignal, splitProps } from 'solid-js';
import { JSX } from 'solid-js/jsx-runtime';
import { useTippy } from 'solid-tippy';
import clsx from 'classnames';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/dist/svg-arrow.css';
import 'tippy.js/animations/shift-away.css';
import styles from './Dropdown.module.scss';

type ContentProps = {
  items: JSX.Element[];
} & JSX.HTMLAttributes<HTMLDivElement>;

const Content = (props: ContentProps) => {
  const [local, otherProps] = splitProps(props, ['items']);
  return (
    <div
      {...otherProps}
      class={clsx(styles['dropdown-list'], otherProps.class)}
    >
      {local.items}
    </div>
  );
};

export type DropdownProps = {
  items: JSX.Element[];
  menu: JSX.Element;
} & JSX.HTMLAttributes<HTMLDivElement>;

const Dropdown = (props: DropdownProps) => {
  const [local, otherProps] = splitProps(props, ['items', 'menu']);
  const [getAnchor, setAnchor] = createSignal<HTMLSpanElement>();

  useTippy(getAnchor, {
    props: {
      theme: 'vanilla',
      content: (<Content items={local.items} {...otherProps} />) as Element,
      interactive: true,
      arrow: true,
      animation: 'shift-away',
      trigger: 'mouseenter click',
    },
    hidden: true,
  });

  return (
    <div style={{ width: 'fit-content', display: 'inline' }} ref={setAnchor}>
      {local.menu}
    </div>
  );
};

export default Dropdown;
