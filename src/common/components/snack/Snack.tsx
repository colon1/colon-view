import { ParentComponent } from 'solid-js';
import clsx from 'classnames';
import commonStyles from '@common/components/common.module.scss';
import Flex from '@common/components/wrapper/Flex';
import snackStyles from './Snack.module.scss';

export type SnackProps = {
  isShowing: boolean;
  title: string;
  description: string;
  icon: string;
};

const Snack: ParentComponent<SnackProps> = (props) => {
  return (
    <>
      {props.isShowing ? (
        <Flex
          center
          row
          class={clsx(
            snackStyles.snack,
            commonStyles.background,
            commonStyles.padding,
            commonStyles.margin,
            commonStyles['fit-content'],
            commonStyles['pointer-auto'],
          )}
        >
          <img
            src={props.icon}
            alt={'snack-icon'}
            class={clsx(snackStyles['snack-icon'])}
            width="2.5em"
            height="2.5em"
          />
          <Flex>
            <Flex row center>
              <Flex top style={{ 'overflow-y': 'scroll' }}>
                <p class={clsx(snackStyles['snack-title'])}>{props.title}</p>
                <p class={clsx(snackStyles['snack-description'])}>
                  {props.description}
                </p>
              </Flex>
              {props.children}
            </Flex>
          </Flex>
        </Flex>
      ) : (
        <></>
      )}
    </>
  );
};

export default Snack;
