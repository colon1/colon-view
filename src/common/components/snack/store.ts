import { createMemo, createSignal } from 'solid-js';

export type SnackStatus = {
  type: 'info' | 'warn' | 'success' | 'failure';
  title: string;
  description: string;
  visibility: boolean;
};

export const [getSnackStatus, setSnackStatus] = createSignal<SnackStatus>({
  type: 'info',
  title: 'Welcome',
  description: 'Welcome to our website 👋',
  visibility: false,
});

const icons: Map<string, string> = new Map();
icons.set('info', 'info_blue_24dp');
icons.set('warn', 'warning_orange_24dp');
icons.set('success', 'task_alt_green_24dp');
icons.set('failure', 'cancel_red_24dp');

export const getSnackIcon = createMemo<string>(() => {
  const status = getSnackStatus();
  return `/colon-view/assets/icons/${icons.get(status.type || 'info')}.svg`;
});
