import { onCleanup } from 'solid-js';
import { EventType } from 'mitt';
import emitter, { Emitter } from '../tools/events';

/**
 * Create an event emitter cleared on disposal or recalculation of the current reactive scope.
 * @returns event emitter
 */
const createEmitter = <
  Events extends Record<EventType, unknown>,
>(): Emitter<Events> => {
  const e = emitter<Events>();
  onCleanup(() => e.clear());
  return e;
};

export default createEmitter;
