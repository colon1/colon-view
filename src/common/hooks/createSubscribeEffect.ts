import { Accessor, createEffect, on, Setter } from 'solid-js';

const createSubscribeEffect = <T extends unknown[]>(
  getters: { [I in keyof T]: Accessor<T[I]> },
  setters: { [I in keyof T]: Setter<T[I]> },
) =>
  getters.forEach((getter, i) =>
    createEffect(on(getter, (value) => setters[i](value))),
  );

export default createSubscribeEffect;
