import createDirtyEffect from './createDirtyEffect';

/**
 * Create animation frame.
 * @param callback callback executed after each frame
 */
const createAnimationFrame = (callback: () => void): void =>
  createDirtyEffect(() => {
    const request = () =>
      requestAnimationFrame(() => {
        request();
        callback();
      });
    const animationFrame = request();
    return () => cancelAnimationFrame(animationFrame);
  });

export default createAnimationFrame;
