import { Accessor } from 'solid-js';
import createDirtyEffect from './createDirtyEffect';

/**
 * Create basic listener triggered on HTML element click.
 * @param getRef ref accessor of the HTML element
 * @param callback listener triggered on HTML element click
 */
const createClickListener = <T extends HTMLElement>(
  getRef: Accessor<T | undefined>,
  callback: (event: MouseEvent) => void,
): void =>
  createDirtyEffect(() => {
    const ref = getRef();
    if (!ref) return;

    const handleClick = (event: MouseEvent) => {
      if (ref === event.target) callback(event);
    };
    document.addEventListener('click', handleClick);

    return () => document.removeEventListener('click', handleClick);
  });

export default createClickListener;
