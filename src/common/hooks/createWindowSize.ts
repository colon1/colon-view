import { createSignal } from 'solid-js';
import { Vector } from '../tools/geometry';
import createDirtyEffect from './createDirtyEffect';

/**
 * Create reactive window size.
 * @param ratio
 * @returns
 */
const createWindowSize = (ratio = devicePixelRatio) => {
  const [getSize, setSize] = createSignal<Vector<2>>([0, 0]);

  createDirtyEffect(() => {
    const onWindowResize = () =>
      setSize([
        Math.floor(window.innerWidth * ratio),
        Math.floor(window.innerHeight * ratio),
      ]);
    onWindowResize();
    window.addEventListener('resize', onWindowResize);
    if (screen?.orientation)
      screen.orientation.addEventListener('change', onWindowResize);

    return () => {
      window.removeEventListener('resize', onWindowResize);
      if (screen?.orientation)
        screen.orientation.removeEventListener('change', onWindowResize);
    };
  });

  return getSize;
};

export default createWindowSize;
