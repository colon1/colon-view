import { createEffect, onCleanup } from 'solid-js';

/**
 * Basically works like createEffect, but needs to be cleaned up.
 * @param effect effect returning a cleanup function
 */
const createDirtyEffect = (effect: () => (() => void) | undefined): void => {
  // last effect cleanup function
  let cleanup: (() => void) | undefined = undefined;

  /**
   * Cleanup the last effect if we need to.
   */
  const tryToCleanup = () => {
    if (cleanup) cleanup();
  };

  createEffect(() => {
    tryToCleanup();
    cleanup = effect();
  });

  onCleanup(tryToCleanup);
};

export default createDirtyEffect;
