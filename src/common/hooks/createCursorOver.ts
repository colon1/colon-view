import { Accessor, createSignal } from 'solid-js';
import createDirtyEffect from './createDirtyEffect';

const createCursorOver = <E extends HTMLElement>(getRef: Accessor<E>) => {
  const [isCursorOverElement, setCursorOverElement] =
    createSignal<boolean>(false);

  createDirtyEffect(() => {
    const element = getRef();
    const onMouse = (e: MouseEvent) =>
      setCursorOverElement(
        document.elementFromPoint(e.clientX, e.clientY) === element,
      );
    document.addEventListener('mousemove', onMouse);
    document.addEventListener('click', onMouse);
    return () => {
      document.removeEventListener('mousemove', onMouse);
      document.removeEventListener('click', onMouse);
    };
  });

  return isCursorOverElement;
};

export default createCursorOver;
