import { Accessor } from 'solid-js';
import { SimpleEmitter } from '@common/tools/events';
import { mobileCheck } from '@common/tools/mobile';
import { Vector } from '@common/tools/geometry';
import createEmitter from './createEmitter';
import createDirtyEffect from './createDirtyEffect';

// TODO: improve this part (typings, clarity & testing)

// pixels threshold before considering mouse dragging
const DRAG_THRESHOLD = 2;

// controls events
export type Events = {
  touchscalestart: TouchEvent;
  touchscalemove: {
    domEvent: TouchEvent;
    extra: { ratio: number; mid: Vector<2> };
  };
  touchscaleend: TouchEvent;
  dragstart:
    | TouchEvent
    | (MouseEvent & {
        lastMouseX: number;
        lastMouseY: number;
        isMouseDown: boolean;
        dx: number;
        dy: number;
      });
  dragmove: {
    domEvent: TouchEvent | MouseEvent;
    extra: { dx: number; dy: number };
  };
  dragend: TouchEvent | MouseEvent;
  keydown: KeyboardEvent;
  docmouseup: MouseEvent;
  docmousemove: MouseEvent;
  click: MouseEvent;
  mousewheel: {
    domEvent: WheelEvent;
    extra: { delta: number; lastMouseX: number; lastMouseY: number };
  };
  dbclick: {
    domEvent: MouseEvent;
    extra: {
      lastMouseX: number;
      lastMouseY: number;
    };
  };
};

type TouchEventListener = (this: unknown, ev: TouchEvent) => unknown;
type KeyboardEventListener = (this: unknown, ev: KeyboardEvent) => unknown;
type MouseEventListener = (this: unknown, ev: MouseEvent) => unknown;
type WheelEventListener = (this: unknown, ev: WheelEvent) => unknown;

/**
 * Create basic controls triggered through an HTML element.
 *
 * - Emitted events:
 *   dragstart, dragmove, dragend,
 *   touchstart, touchmove, touchend, touchscalestart, touchscalemove, touchscaleend,
 *   keydown, click, dbclick, mousewheel, docmouseup, docmousemove
 *
 * @param getRef ref accessor of the HTML element
 * @returns simple event emitter
 */
const createControls = <E extends HTMLElement>(
  getRef: Accessor<E>,
): SimpleEmitter<Events> => {
  const { on, off, clear, emit } = createEmitter<Events>();

  createDirtyEffect(() => {
    const elementRef = getRef();

    const mobile = mobileCheck();
    let lastMouseX = window.innerWidth / 2;
    let lastMouseY = window.innerHeight / 2;

    let touchStartEventListener: TouchEventListener;
    let touchMoveEventListener: TouchEventListener;
    let touchEndEventListener: TouchEventListener;
    let docKeyDownEventListener: KeyboardEventListener;

    // touch / keyboard
    if (mobile) {
      let isScaling = false;
      let initSqrDist: number;
      // let touchCount = 0;
      const pinchStart = (event: TouchEvent) => {
        initSqrDist =
          (event.touches[0].pageX - event.touches[1].pageX) *
            (event.touches[0].pageX - event.touches[1].pageX) +
          (event.touches[0].pageY - event.touches[1].pageY) *
            (event.touches[0].pageY - event.touches[1].pageY);
        emit('touchscalestart', event);
      };
      const pinchMove = (event: TouchEvent) => {
        const ratio =
          ((event.touches[0].pageX - event.touches[1].pageX) *
            (event.touches[0].pageX - event.touches[1].pageX) +
            (event.touches[0].pageY - event.touches[1].pageY) *
              (event.touches[0].pageY - event.touches[1].pageY)) /
          initSqrDist;
        const mid: Vector<2> = [
          (event.touches[0].pageX + event.touches[1].pageX) / 2,
          (event.touches[0].pageY + event.touches[1].pageY) / 2,
        ];
        emit('touchscalemove', { domEvent: event, extra: { ratio, mid } });
      };
      const pinchEnd = (event: TouchEvent) => {
        emit('touchscaleend', event);
      };
      touchStartEventListener = (event) => {
        // touchCount = event.touches.length;
        if (event.touches.length === 2) {
          isScaling = true;
          pinchStart(event);
        } else {
          emit('dragstart', event);
          lastMouseX = event.touches[0].pageX;
          lastMouseY = event.touches[0].pageY;
        }
      };
      touchMoveEventListener = (event) => {
        event.preventDefault();
        // touchCount = event.touches.length;
        if (isScaling) {
          pinchMove(event);
        } else {
          const dx = event.touches[0].pageX - lastMouseX;
          const dy = event.touches[0].pageY - lastMouseY;
          lastMouseX = event.touches[0].pageX;
          lastMouseY = event.touches[0].pageY;
          emit('dragmove', { domEvent: event, extra: { dx, dy } });
        }
      };
      touchEndEventListener = (event) => {
        // touchCount = event.touches.length;
        if (isScaling) {
          pinchEnd(event);
          isScaling = false;
          lastMouseX = event.touches[0].pageX;
          lastMouseY = event.touches[0].pageY;
        } else {
          emit('dragend', event);
        }
      };
      elementRef.addEventListener('touchstart', touchStartEventListener, false);
      elementRef.addEventListener('touchmove', touchMoveEventListener, false);
      elementRef.addEventListener('touchend', touchEndEventListener, false);
    } else {
      // keyboard
      docKeyDownEventListener = (event: KeyboardEvent) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (event.target.matches('input')) return;
        emit('keydown', event);
      };
      document.addEventListener('keydown', docKeyDownEventListener);
    }

    // mouse
    let isMouseMoving = 0;
    let isDragging = false;
    let isMouseDown = false;
    const docMouseUpEventListener: MouseEventListener = (event: MouseEvent) => {
      isMouseDown = false;
      if (isDragging) {
        emit('dragend', event);
      }
      isDragging = false;
      emit('docmouseup', event);
    };
    const mouseUpEventListener: MouseEventListener = (event: MouseEvent) => {
      isMouseDown = false;
      if (isMouseMoving < DRAG_THRESHOLD) {
        emit('click', event);
      }
    };
    const mouseDownEventListener: MouseEventListener = () => {
      isMouseDown = true;
      isMouseMoving = 0;
    };
    const mouseWheelEventListener: WheelEventListener = (event: WheelEvent) => {
      emit('mousewheel', {
        domEvent: event,
        extra: { delta: event.deltaY, lastMouseX, lastMouseY },
      });
    };
    const dblclickEventListener: MouseEventListener = (event: MouseEvent) => {
      emit('dbclick', { domEvent: event, extra: { lastMouseX, lastMouseY } });
    };
    const docMouseMoveEventListener: MouseEventListener = (
      event: MouseEvent,
    ) => {
      if (event === undefined) return;

      const dx = event.x - lastMouseX;
      const dy = event.y - lastMouseY;
      if (isMouseDown) {
        isMouseMoving += 1;
        if (isMouseMoving > DRAG_THRESHOLD) {
          if (!isDragging) {
            isDragging = true;
            emit('dragstart', {
              ...event,
              lastMouseX,
              lastMouseY,
              isMouseDown,
              dx,
              dy,
            });
          } else {
            emit('dragmove', { domEvent: event, extra: { dx, dy } });
          }
        }
      }
      emit('docmousemove', event);

      if (isDragging) {
        if (event.stopPropagation) event.stopPropagation();
        if (event.preventDefault) event.preventDefault();
        event.cancelBubble = true;
        event.returnValue = false;
        window.getSelection()?.removeAllRanges();
      }

      lastMouseX = event.x;
      lastMouseY = event.y;
    };
    document.addEventListener('mouseup', docMouseUpEventListener, false);
    elementRef.addEventListener('mouseup', mouseUpEventListener, false);
    elementRef.addEventListener('mousedown', mouseDownEventListener, false);
    elementRef.addEventListener('wheel', mouseWheelEventListener, {
      passive: true,
    });
    elementRef.addEventListener('dblclick', dblclickEventListener);
    document.addEventListener('mousemove', docMouseMoveEventListener, false);

    return () => {
      if (mobile) {
        elementRef.removeEventListener('touchstart', touchStartEventListener);
        elementRef.removeEventListener('touchmove', touchMoveEventListener);
        elementRef.removeEventListener('touchend', touchEndEventListener);
      } else document.removeEventListener('keydown', docKeyDownEventListener);
      document.removeEventListener('mouseup', docMouseUpEventListener);
      elementRef.removeEventListener('mouseup', mouseUpEventListener);
      elementRef.removeEventListener('mousedown', mouseDownEventListener);
      elementRef.removeEventListener('wheel', mouseWheelEventListener);
      elementRef.removeEventListener('dblclick', dblclickEventListener);
      document.removeEventListener('mousemove', docMouseMoveEventListener);
    };
  });

  return { on, off, clear };
};

export default createControls;
