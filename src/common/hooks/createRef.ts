import { createSignal, Signal } from 'solid-js';

/**
 * Create ref signal.
 * @returns ref signal
 */
const createRef = <E extends Element>(): Signal<E | undefined> => {
  const [getter, setter] = createSignal<E | undefined>();
  return [getter, setter];
};
export default createRef;
