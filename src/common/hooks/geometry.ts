import { Accessor, createSignal, createEffect, createMemo } from 'solid-js';
import { clamp } from '../tools/utils';
import { Vector, Rectangle, add, apply, dot, fixRect } from '../tools/geometry';

/**
 * Create a reactive spacially limited vector.
 * @param getVector vector accessor
 * @param getLimits spacial limits
 * @returns spacially limited vector accessor
 */
export const createClampedVector = <N extends number>(
  getVector: Accessor<Vector<N>>,
  getLimits: Accessor<Rectangle<N>>,
): Accessor<Vector<N>> => {
  const getClampedVector = createMemo(() => {
    const currentVector = getVector();
    const [min, max] = getLimits();
    return apply(currentVector, (x, i) => clamp(min[i], x, max[i]));
  });
  return getClampedVector;
};

/**
 * Create a reactive clamped scalar.
 * @param getScalar scalar accessor
 * @param getLimits clamp limits
 * @returns clamped scalar accessor
 */
export const createClampedScalar = (
  getScalar: Accessor<number>,
  getLimits: Accessor<Vector<2>>,
): Accessor<number> => {
  const getClampledScalar = createMemo(() => {
    const currentValue = getScalar();
    const [min, max] = getLimits();
    return clamp(min, currentValue, max);
  });
  return getClampledScalar;
};

/**
 * Create a reactive rectangle based on its properties.
 * @param getPosition rectangle position accessor
 * @param getZoom rectangle zoom accessor
 * @param getBounds rectangle bounds accessor
 * @returns rectangle accessor
 */
export const createRectangle = (
  getPosition: Accessor<Vector<2>>,
  getZoom: Accessor<number>,
  getBounds: Accessor<Vector<2>>,
): Accessor<Rectangle<2>> => {
  const [getRectangle, setRectangle] = createSignal<Rectangle<2>>([
    [0, 0],
    [0, 0],
  ]);

  createEffect(() => {
    const position = getPosition();
    const zoom = getZoom();
    const bounds = getBounds();
    setRectangle([
      add(position, dot(bounds, -1 / (2 * zoom))),
      add(position, dot(bounds, 1 / (2 * zoom))),
    ] as Rectangle<2>);
  });

  return getRectangle;
};

/**
 * Create reactive rectangle position limits based on its properties.
 * @param getPositionLimits position limits accessor
 * @param getZoom rectangle zoom accessor
 * @param getBounds rectangle bounds accessor
 * @returns rectangle position limits accessor
 */
export const createRectanglePositionLimits = (
  getPositionLimits: Accessor<Rectangle<2>>,
  getZoom: Accessor<number>,
  getBounds: Accessor<Vector<2>>,
): Accessor<Rectangle<2>> => {
  const [getRectanglePositionLimits, setRectanglePositionLimits] = createSignal<
    Rectangle<2>
  >([
    [0, 0],
    [0, 0],
  ]);

  createEffect(() => {
    const positionLimits = getPositionLimits();
    const zoom = getZoom();
    const bounds = getBounds();
    setRectanglePositionLimits(
      fixRect<2>([
        add(positionLimits[0], dot(bounds, 1 / (2 * zoom))),
        add(positionLimits[1], dot(bounds, -1 / (2 * zoom))),
      ]),
    );
  });

  return getRectanglePositionLimits;
};
