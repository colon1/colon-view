import { ethers } from 'ethers';
import { Accessor, createMemo } from 'solid-js';

/**
 * Create the Web3 Provider accessor if one is available.
 * @returns Web3 Provider accessor if one is available
 */
const createProvider = (): Accessor<
  ethers.providers.Web3Provider | undefined
> => {
  const getProvider = createMemo<ethers.providers.Web3Provider | undefined>(
    () => {
      try {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return new ethers.providers.Web3Provider(window.ethereum);
      } catch (err) {
        console.warn('No Web3 provider found.', err);
        return undefined;
      }
    },
  );
  return getProvider;
};

export default createProvider;
