import { ethers } from 'ethers';
import { Accessor, createMemo } from 'solid-js';

const createContract = <T extends ethers.Contract>(
  getContractAddress: Accessor<string>,
  getContractInterface: Accessor<ethers.ContractInterface>,
  getProvider: Accessor<ethers.Signer | ethers.providers.Provider>,
): Accessor<T> => {
  const getContract = createMemo(
    () =>
      new ethers.Contract(
        getContractAddress(),
        getContractInterface(),
        getProvider(),
      ),
  );
  return getContract as Accessor<T>;
};

export default createContract;
