import { Accessor, createMemo, on } from 'solid-js';

/**
 * Memo computed only when all the reactive dependencies are well defined.
 * @param deps list of reactive dependencies
 * @param fn callback called once all the reactive dependencies are well defined
 */
const createOnDefined = <T, S>(
  deps: Accessor<S>[],
  fn: () => T,
): Accessor<T | undefined> => {
  const areDefined = createMemo(() =>
    deps
      .map((getValue) => getValue() !== undefined)
      .reduce((acc, cur) => acc && cur),
  );
  const getHookResult = createMemo<T | undefined>(
    on([areDefined], ([defined]) => (defined ? fn() : undefined)),
  );
  return getHookResult;
};

export default createOnDefined;
