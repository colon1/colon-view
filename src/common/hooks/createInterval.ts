import { Accessor } from 'solid-js';
import createDirtyEffect from './createDirtyEffect';

/**
 * Create interval.
 * @param callback callback executed after delay
 * @param getDelay delay accessor of the interval (in ms)
 */
const createInterval = (
  callback: TimerHandler,
  getDelay: Accessor<number>,
): void =>
  createDirtyEffect(() => {
    const delay = getDelay();
    const interval = setInterval(callback, delay);
    return () => clearInterval(interval);
  });

export default createInterval;
