import { Accessor, createMemo } from 'solid-js';

/**
 * Wrap an accessor so that it returns `undefined` when the accessor itself is `undefined`.
 * @param getAccessor accessor accessor
 * @returns computed value or `undefined`
 */
const wrapAccessor = <T>(getAccessor: Accessor<Accessor<T> | undefined>) => {
  const getResult = createMemo<T | undefined>(() => {
    const getValue = getAccessor();
    return getValue ? getValue() : undefined;
  });
  return getResult;
};

export default wrapAccessor;
