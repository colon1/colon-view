import { Accessor, createMemo } from 'solid-js';
import { getCanvasContext2D } from '@common/tools/canvas';

const createCanvasContext2D = (
  getCanvas: Accessor<HTMLCanvasElement>,
): Accessor<CanvasRenderingContext2D> => {
  const getContext = createMemo(() => {
    return getCanvasContext2D(getCanvas());
  });
  return getContext;
};

export default createCanvasContext2D;
