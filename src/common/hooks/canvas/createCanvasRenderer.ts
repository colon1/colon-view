import { Accessor, createMemo } from 'solid-js';
import createCanvasContext2D from '@common/hooks/canvas/createCanvasContext2D';
import createEmitter from '../createEmitter';

export type RenderingEvents = {
  mkdirty: undefined;
};

export const createMkDirty = (): [() => void, (cb: () => void) => void] => {
  const rendererEmitter = createEmitter<RenderingEvents>();
  const mkDirty = () => rendererEmitter.emit('mkdirty');
  const onDirty = (cb: () => void) => rendererEmitter.on('mkdirty', cb);
  return [mkDirty, onDirty];
};

export type CanvasDrawer = (context: CanvasRenderingContext2D) => void;
export type CanvasRenderer = { mkDirty: () => void };

/**
 * Draw on the canvas next animation frame when it's marked as 'dirty'.
 * @param getCanvas HTML canvas element accessor
 * @param getDrawer canvas drawer accessor
 * @returns canvas renderer accessor
 */
const createCanvasRenderer = (
  getCanvas: Accessor<HTMLCanvasElement>,
  getDrawer: Accessor<CanvasDrawer>,
): Accessor<CanvasRenderer> => {
  const getContext = createCanvasContext2D(getCanvas);
  const getCanvasRenderer = createMemo<CanvasRenderer>(() => {
    const draw = getDrawer();
    const context = getContext();

    // internal renderer properties
    let isRendering = false;
    let isDirty = false;

    /**
     * Mark the renderer canvas "dirty", triggering next frame render.
     */
    const mkDirty = () => {
      const nextFrame = () => {
        draw(context);
        if (isDirty) requestAnimationFrame(nextFrame);
        else isRendering = false;
        isDirty = false;
      };

      isDirty = isRendering;
      if (!isDirty) {
        requestAnimationFrame(nextFrame);
        isRendering = true;
      }
    };

    mkDirty();

    return { mkDirty };
  });
  return getCanvasRenderer;
};

export default createCanvasRenderer;
