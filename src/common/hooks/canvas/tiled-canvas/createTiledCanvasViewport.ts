import { Accessor, createMemo } from 'solid-js';
import createWindowSize from '@common/hooks/createWindowSize';
import { dot, Rectangle, Vector } from '@common/tools/geometry';
import createViewport, { Viewport } from '../viewport/createViewport';

/**
 * Create the viewport associated to a tiled canvas.
 * @param getTileSize
 * @param getTileBounds
 * @returns viewport associated to a tiled canvas
 */
const createTiledCanvasViewport = (
  getPosition: Accessor<Vector<2>>,
  getZoom: Accessor<number>,
  getTileSize: Accessor<number>,
  getTileBounds: Accessor<Vector<2>>,
): Accessor<Viewport> => {
  const getPositionLimits = createMemo<Rectangle<2>>(() => {
    const totalHalfSize = dot(getTileBounds(), getTileSize() / 2);
    return [dot(totalHalfSize, -1), totalHalfSize];
  });
  const getZoomLimits = createMemo<Vector<2>>(() => [1, 64]);
  return createViewport(
    getPosition,
    getZoom,
    createWindowSize(),
    getPositionLimits,
    getZoomLimits,
  );
};

export default createTiledCanvasViewport;
