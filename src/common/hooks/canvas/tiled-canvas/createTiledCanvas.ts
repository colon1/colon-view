import { Accessor, createEffect, createMemo } from 'solid-js';
import {
  RGBPixel,
  setCanvasPixels,
  getCanvasPixels,
  createCanvas,
  getCanvasContext2D,
} from '@common/tools/canvas';
import { SimpleEmitter } from '@common/tools/events';
import {
  apply,
  dot,
  equals,
  mod,
  Rectangle,
  Vector,
} from '@common/tools/geometry';
import { groupBy, clamp } from '@common/tools/utils';
import createEmitter from '@common/hooks/createEmitter';
import ensure from '@common/tools/ensure';

export type Tile = ImageData | HTMLImageElement | HTMLCanvasElement;

export type TileLoader = (indexes: Vector<2>[]) => Promise<Tile>[];

export type TiledCanvasEvents = {
  loaded: Vector<2>;
};

export type TiledCanvas = {
  getViewRectangle: Accessor<Rectangle<2>>;
  getTileSize: Accessor<number>;
  getTileBounds: Accessor<Vector<2>>;
  setPixels: (pixels: RGBPixel[]) => void;
  getPixels: (indexes: Vector<2>[]) => RGBPixel[];
  loadTiles: (indexes?: Vector<2>[]) => void;
  draw: (context: CanvasRenderingContext2D, indexes?: Vector<2>[]) => void;
} & SimpleEmitter<TiledCanvasEvents>;

/**
 * Create a reactive tiled canvas fetching tiles according to a view rectangle.
 * @param getViewRectangle a view rectangle accessor
 * @param getTileSize tile size (in pixels) accessor
 * @param getTileBounds tile bounds (in tiles) accessor
 * @param getTileLoader tile data loader accessor
 * @param mkDirty optional renderer dirty marker
 * @returns reactive tiled canvas accessor
 */
const createTiledCanvas = (
  getViewRectangle: Accessor<Rectangle<2>>,
  getTileSize: Accessor<number>,
  getTileBounds: Accessor<Vector<2>>,
  getTileLoader: Accessor<TileLoader>,
  mkDirty?: () => void,
): Accessor<TiledCanvas> => {
  const { on, off, clear, emit } = createEmitter<TiledCanvasEvents>();

  const getTiledCanvas = createMemo<TiledCanvas>(() => {
    const tileSize = getTileSize();
    const tileBounds = getTileBounds();
    const tileLoader = getTileLoader();

    // set of tiles
    let visibleTilesIndex: Vector<2>[] = [];
    const queuedTiles = new Map<string, number>();
    const loadedTiles = new Map<
      string,
      {
        canvas: HTMLCanvasElement;
        loaded: number;
      }
    >();

    // some utils
    const keyOf = (index: Vector<2>) => index.join(':');
    const indexInTileOf = (index: Vector<2>) => mod(index, tileSize);
    const tileIndexOf = (index: Vector<2>) =>
      apply(dot(index, 1 / tileSize), Math.floor);

    /**
     * Set pixels color.
     * @param pixels RGB pixels array
     */
    const setPixels = (pixels: RGBPixel[]) =>
      groupBy(pixels, ({ index }) => keyOf(tileIndexOf(index)))
        .filter(([tileKey]) => loadedTiles.has(tileKey))
        .forEach(([tileKey, pixels]) => {
          setCanvasPixels(
            ensure(loadedTiles.get(tileKey)).canvas,
            pixels.map(({ value, index }) => ({
              value,
              index: indexInTileOf(index),
            })),
          );
        });

    /**
     * Get pixels color.
     * @param indexes pixels index
     * @returns RGB pixels array
     */
    const getPixels = (indexes: Vector<2>[]): RGBPixel[] =>
      groupBy(indexes, (index) => keyOf(tileIndexOf(index)))
        .filter(([tileKey]) => loadedTiles.has(tileKey))
        .map(([tileKey, indexes]) =>
          getCanvasPixels(
            ensure(loadedTiles.get(tileKey)).canvas,
            indexes.map((index) => indexInTileOf(index)),
          ),
        )
        .flat();

    /**
     * Load (visible) tiles.
     * @param indexes tiles index to load (i.e. visible tiles)
     */
    const loadTiles = (indexes: Vector<2>[] = visibleTilesIndex) => {
      // filter loaded tiles / recently fetched tiles
      const now = Date.now();
      indexes = indexes
        .filter((index) => !loadedTiles.has(keyOf(index)))
        .filter((index) => {
          const queuedTile = queuedTiles.get(keyOf(index));
          return queuedTile === undefined || now - queuedTile > 20000;
        });

      // load concerned unloaded tiles image data
      const promises = tileLoader(indexes);
      promises.forEach((imageDataPromise, i) => {
        const key = keyOf(indexes[i]);
        queuedTiles.set(key, now);
        imageDataPromise
          .then((image) => {
            const canvas = createCanvas(tileSize, tileSize);
            const context = getCanvasContext2D(canvas);
            if (image instanceof ImageData) {
              context.imageSmoothingEnabled = false;
              context.putImageData(image, 0, 0);
            } else context.drawImage(image, 0, 0);
            loadedTiles.set(key, { canvas, loaded: Date.now() });
            if (mkDirty) mkDirty();
            emit('loaded', indexes[i]);
          })
          .catch((err) => console.error('Could not laod tile.', err));
      });
    };

    /**
     * Draw (visible) tiles.
     * @param context canvas rendering context 2D
     * @param mkDirty mark canvas as dirty, triggering next render frame
     * @param indexes tiles index to draw (i.e. visible tiles)
     */
    const draw = (
      context: CanvasRenderingContext2D,
      indexes: Vector<2>[] = visibleTilesIndex,
    ) => {
      const prevAlpha = context.globalAlpha;
      indexes.forEach(([x, y]) => {
        const tile = loadedTiles.get(keyOf([x, y]));
        if (tile && tile.canvas) {
          context.globalAlpha *= clamp(0, (Date.now() - tile.loaded) / 200, 1);
          context.drawImage(tile.canvas, x * tileSize, y * tileSize);
          context.globalAlpha = prevAlpha;
          if (mkDirty && context.globalAlpha < 1) mkDirty();
        } else {
          context.globalAlpha = 1;
          context.fillStyle = `rgba(50, 50, 50, ${
            0.2 + 0.2 * Math.sin(0.15 * (x + y) + 0.5 * Date.now() * 1e-2) ** 2
          })`;
          context.fillRect(x * tileSize, y * tileSize, tileSize, tileSize);
          context.globalAlpha = prevAlpha;
          if (mkDirty) mkDirty();
        }
      });
    };

    let prevC1: Vector<2> | null = null;
    let prevC2: Vector<2> | null = null;
    createEffect(() => {
      const [p1, p2] = getViewRectangle();
      const c1 = apply(p1, (x) => Math.floor(x / tileSize));
      const c2 = apply(p2, (x) => Math.ceil(x / tileSize));
      if (prevC1 && prevC2 && equals(c1, prevC1) && equals(c2, prevC2)) return;
      const [width, height] = tileBounds;

      const indexes: Vector<2>[] = [];
      for (let cx = c1[0]; cx < c2[0]; cx++)
        for (let cy = c1[1]; cy < c2[1]; cy++)
          if (
            cx >= -width / 2 &&
            cx < width / 2 &&
            cy >= -height / 2 &&
            cy < height / 2
          )
            indexes.push([cx, cy] as Vector<2>);

      visibleTilesIndex = indexes;
      loadTiles();
      prevC1 = c1 as Vector<2>;
      prevC2 = c2 as Vector<2>;
    });

    return {
      getViewRectangle,
      getTileSize,
      getTileBounds,
      setPixels,
      getPixels,
      loadTiles,
      draw,
      on,
      off,
      clear,
    };
  });

  return getTiledCanvas;
};

export default createTiledCanvas;
