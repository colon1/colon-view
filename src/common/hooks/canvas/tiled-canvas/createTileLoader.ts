import { Accessor, createMemo } from 'solid-js';
import { Vector } from '@common/tools/geometry';
import { TileLoader } from './createTiledCanvas';

const createTileLoader = (
  getTileSize: Accessor<number>,
  getTileSourceLoader: Accessor<(index: Vector<2>) => string>,
) => {
  const getTileLoader = createMemo<TileLoader>(() => {
    const tileSize = getTileSize();
    const loadTileSource = getTileSourceLoader();
    return (indexes: Vector<2>[]) =>
      indexes.map(
        (index) =>
          new Promise((resolve) => {
            const tileImage = new Image(tileSize, tileSize);
            tileImage.src = loadTileSource(index);
            tileImage.crossOrigin = 'Anonymous';
            tileImage.onload = () => resolve(tileImage);
          }),
      );
  });
  return getTileLoader;
};

export default createTileLoader;
