import { Accessor, createEffect } from 'solid-js';
import { smooth2, smooth } from '@common/tools/animation';
import { clone, equals, n2sqr, sub, Vector } from '@common/tools/geometry';
import { outPow2, outPow } from '@common/tools/interpolation';

export type Cursor = {
  getPosition: Accessor<Vector<2>>;
  getVisibility: Accessor<boolean>;
  getColor: Accessor<string>;
  getSize: Accessor<number>;
  click: () => void;
  draw: (context: CanvasRenderingContext2D) => void;
};

/**
 * Create a cursor.
 * @param getPosition
 * @param getVisibility
 * @param getColor
 * @param getSize
 * @param mkDirty renderer dirty marker
 * @returns
 */
const createCursor = (
  getPosition: Accessor<Vector<2>>,
  getVisibility: Accessor<boolean>,
  getColor: Accessor<string>,
  getSize: Accessor<number>,
  mkDirty: () => void,
): Accessor<Cursor> => {
  const defaultCursorBorder = 0.1;
  const defaultCursorOpacity = 1;
  const defaultCursorPosition = [0, 0] as Vector<2>;
  const smoothCursorPositionDuration = 0.1;
  const smoothCursorBorderDuration = 0.2;
  const smoothCursorOpacityDuration = 0.1;

  let currentPosition = clone(defaultCursorPosition);
  const smoothMove = smooth2(
    (vec) => {
      currentPosition = clone(vec);
      mkDirty();
    },
    outPow2(3),
    defaultCursorPosition,
    smoothCursorPositionDuration,
  );

  let currentOpacity = defaultCursorOpacity;
  const smoothOpacity = smooth(
    (value) => {
      currentOpacity = value;
      mkDirty();
    },
    outPow(3),
    defaultCursorOpacity,
    smoothCursorOpacityDuration,
  );

  let currentBorder = defaultCursorBorder;
  const smoothBorder = smooth(
    (value) => {
      currentBorder = value;
      mkDirty();
    },
    outPow(3),
    defaultCursorBorder,
    smoothCursorBorderDuration,
  );

  createEffect<Vector<2>>(
    (lastPosition) => {
      const position = getPosition();
      if (!equals(lastPosition, position)) {
        if (n2sqr(sub(lastPosition, position)) > 2) currentPosition = position;
        else smoothMove(position, lastPosition);
      }
      return clone(position);
    },
    [0, 0],
  );

  createEffect(() => {
    smoothOpacity(getVisibility() ? 1 : 0);
  });

  /**
   * Make the cursor click.
   */
  const click = () => {
    smoothBorder(defaultCursorBorder, 5 * defaultCursorBorder);
  };

  /**
   * Draw the cursor.
   */
  const draw = (context: CanvasRenderingContext2D) => {
    // context.beginPath();
    // context.lineWidth = currentBorder;
    const isVisible = currentOpacity > 1e-1;
    const size = getSize();
    context.globalAlpha = currentOpacity;
    if (isVisible)
      context.globalAlpha *= 0.7 + 0.5 * Math.sin(Date.now() * 1e-2);
    context.fillStyle = getColor();
    context.fillRect(
      size * currentPosition[0],
      size * currentPosition[1],
      size,
      size,
    );
    if (isVisible) mkDirty();
    //   context.closePath();
    //   context.stroke();
  };

  return () => ({
    getPosition,
    getVisibility,
    getColor,
    getSize,
    click,
    draw,
  });
};

export default createCursor;
