import { Accessor, createSignal } from 'solid-js';
import createControls from '@common/hooks/createControls';
import { apply, equals, Vector } from '@common/tools/geometry';
import { Viewport } from './createViewport';
import viewportProjection from './viewportProjection';

/**
 * Create projected mouse cursor position.
 * @param getRef ref accessor of the HTML element
 * @param getViewport viewport accessor
 * @param shouldUpdate optional function telling if the cursor position should be updated or not
 * @returns projected mouse cursor position
 */
const createViewportProjection = <E extends HTMLElement>(
  getRef: Accessor<E>,
  getViewport: Accessor<Viewport>,
  shouldUpdate?: () => boolean,
): Accessor<Vector<2>> => {
  const [getProjection, setProjection] = createSignal<Vector<2>>([0, 0]);

  createControls<E>(getRef).on('docmousemove', ({ clientX, clientY }) => {
    if (shouldUpdate && shouldUpdate()) return;
    const current = apply(
      viewportProjection(getViewport(), [
        clientX / window.innerWidth,
        clientY / window.innerHeight,
      ]),
      Math.floor,
    );
    const projection = getProjection();
    if (!equals(projection, current)) setProjection(current);
  });

  return getProjection;
};

export default createViewportProjection;
