import { Accessor } from 'solid-js';
import { Rectangle, Vector } from '@common/tools/geometry';
import {
  createClampedScalar,
  createClampedVector,
  createRectangle,
  createRectanglePositionLimits,
} from '@common/hooks/geometry';

export type Viewport = {
  getBounds: Accessor<Vector<2>>;
  getPositionLimits: Accessor<Rectangle<2>>;
  getZoomLimits: Accessor<Vector<2>>;
  getPosition: Accessor<Vector<2>>;
  getZoom: Accessor<number>;
  getRectangle: Accessor<Rectangle<2>>;
};

/**
 * Create a viewport.
 * @param getBounds
 * @param getPositionLimits
 * @param getZoomLimits
 * @param initialPosition optional initial position
 * @param initialZoom optional initial zoom
 * @returns viewport
 */
const createViewport = (
  getPosition: Accessor<Vector<2>>,
  getZoom: Accessor<number>,
  getBounds: Accessor<Vector<2>>,
  getPositionLimits: Accessor<Rectangle<2>>,
  getZoomLimits: Accessor<Vector<2>>,
): Accessor<Viewport> => {
  const getClampedZoom = createClampedScalar(getZoom, getZoomLimits);
  const getRectanglePositionLimits = createRectanglePositionLimits(
    getPositionLimits,
    getClampedZoom,
    getBounds,
  );
  const getLimitedPosition = createClampedVector<2>(
    getPosition,
    getRectanglePositionLimits,
  );
  const getRectangle = createRectangle(
    getLimitedPosition,
    getClampedZoom,
    getBounds,
  );

  return () => ({
    getBounds,
    getPositionLimits,
    getZoomLimits,
    getPosition: getLimitedPosition,
    getZoom: getClampedZoom,
    getRectangle,
  });
};

export default createViewport;
