import { Accessor, createSignal } from 'solid-js';
import { add, dot, hadam, n2sqr, sub, Vector } from '@common/tools/geometry';
import { outPow, outPow2 } from '@common/tools/interpolation';
import createControls from '@common/hooks/createControls';
import { smooth, smooth2 } from '@common/tools/animation';
import limiter from '@common/tools/limiter';
import { clamp } from '@common/tools/utils';
import { Viewport } from './createViewport';

// motion speed
const SPEED = 10;

export type ViewControls = [Accessor<Vector<2>>, Accessor<number>];

/**
 * Create basic viewport controls.
 * @param getRef ref accessor of the HTML element
 * @param getViewport viewport accessor
 */
const createViewportControls = <E extends HTMLElement>(
  getRef: Accessor<E>,
  getViewport: Accessor<Viewport>,
): ViewControls => {
  const [getPosition, setPosition] = createSignal<Vector<2>>([0, 0]);
  const [getZoom, setZoom] = createSignal<number>(1);
  const translate = (vec: Vector<2>) =>
    setPosition(() => add(getViewport().getPosition(), vec));

  const controls = createControls<E>(getRef);

  const damping = 0.5;
  const bonding = 20;
  const moveLimiter = limiter(5);
  let lastMove: Vector<2> = [0, 0];
  const smoothMove = smooth2(translate, outPow2(3), [0, 0], 1);

  const zoomLimiter = limiter(20);
  let targetZoom = 1;
  let originZoom = [0.5, 0.5] as Vector<2>;
  const instantZoom = (zoom: number) => {
    const viewport = getViewport();
    const currentZoom = viewport.getZoom();
    const currentBounds = viewport.getBounds();
    const vec = hadam(currentBounds, sub(originZoom, [0.5, 0.5]));
    const coef = (zoom - currentZoom) / (zoom * currentZoom);
    setZoom(zoom);
    translate(dot(vec, coef));
  };
  const smoothZoom = smooth(instantZoom, outPow(3), 1, 0.25);

  controls.on('dragmove', ({ extra }) => {
    document.body.style.cursor = 'move';
    const viewport = getViewport();
    const zoom = viewport.getZoom();
    const currentMove: Vector<2> = [
      (-extra.dx * devicePixelRatio) / zoom,
      (-extra.dy * devicePixelRatio) / zoom,
    ];
    lastMove = add(dot(lastMove, damping), dot(currentMove, 1 - damping));
    translate(currentMove);
  });

  controls.on('dragend', () => {
    document.body.style.cursor = 'auto';
    const zoom = getViewport().getZoom();
    if (n2sqr(lastMove) > bonding / zoom ** 2) smoothMove([0, 0], lastMove);
    lastMove = [0, 0];
  });

  controls.on('keydown', (event) => {
    if (moveLimiter.limit()) return;
    const zoom = getViewport().getZoom();
    switch (event.code) {
      case 'ArrowLeft':
        smoothMove([0, 0], (lastMove = [-SPEED / zoom, 0]));
        break;
      case 'KeyA':
        smoothMove([0, 0], (lastMove = [-SPEED / zoom, 0]));
        break;
      case 'ArrowUp':
        smoothMove([0, 0], (lastMove = [0, -SPEED / zoom]));
        break;
      case 'KeyW':
        smoothMove([0, 0], (lastMove = [0, -SPEED / zoom]));
        break;
      case 'ArrowRight':
        smoothMove([0, 0], (lastMove = [SPEED / zoom, 0]));
        break;
      case 'KeyD':
        smoothMove([0, 0], (lastMove = [SPEED / zoom, 0]));
        break;
      case 'ArrowDown':
        smoothMove([0, 0], (lastMove = [0, SPEED / zoom]));
        break;
      case 'KeyS':
        smoothMove([0, 0], (lastMove = [0, SPEED / zoom]));
        break;
    }
  });

  controls.on('mousewheel', ({ domEvent, extra }) => {
    if (zoomLimiter.limit()) return;
    try {
      const [min, max] = getViewport().getZoomLimits();
      targetZoom = clamp(min, (extra.delta < 0 ? 2 : 0.5) * targetZoom, max);
      const currentTarget = domEvent.currentTarget as Element;
      const { width, height } = currentTarget.getBoundingClientRect();
      originZoom = [extra.lastMouseX / width, extra.lastMouseY / height];
      smoothZoom(targetZoom);
    } catch (err) {
      console.error('Cannot zoom, invalid element', err);
    }
  });

  let startingZoom = getViewport().getZoom();
  controls.on('touchscalestart', () => {
    startingZoom = getViewport().getZoom();
  });
  controls.on('touchscalemove', ({ domEvent, extra }) => {
    const [min, max] = getViewport().getZoomLimits();
    const logZoom = Math.log(startingZoom * extra.ratio) / Math.log(2);
    const logSnap = Math.round(logZoom);
    targetZoom = 2 ** (Math.abs(logZoom - logSnap) < 0.1 ? logSnap : logZoom);
    targetZoom = clamp(min, targetZoom, max);
    const currentTarget = domEvent.currentTarget as Element;
    const { width, height } = currentTarget.getBoundingClientRect();
    originZoom = [extra.mid[0] / width, extra.mid[1] / height];
    instantZoom(targetZoom);
  });

  controls.on('dbclick', ({ domEvent, extra }) => {
    try {
      targetZoom = 64;
      const currentTarget = domEvent.currentTarget as Element;
      const { width, height } = currentTarget.getBoundingClientRect();
      originZoom = [extra.lastMouseX / width, extra.lastMouseY / height];
      smoothZoom(targetZoom);
    } catch (err) {
      console.error('Cannot zoom, invalid element', err);
    }
  });

  return [getPosition, getZoom];
};

export default createViewportControls;
