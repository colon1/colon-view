import { Viewport } from './createViewport';

/**
 * Canvas rendering context transformation for drawing visible things in the viewport.
 * @param viewport
 * @param context
 */
const viewportTransform = (
  viewport: Viewport,
  context: CanvasRenderingContext2D,
): void => {
  const [width, height] = viewport.getBounds();
  const [x, y] = viewport.getPosition();
  const zoom = viewport.getZoom();
  context.translate(Math.round(width / 2), Math.round(height / 2));
  context.scale(zoom, zoom);
  context.translate(-x, -y);
};

export default viewportTransform;
