import { Vector, add, sub, hadam, dot } from '@common/tools/geometry';
import { Viewport } from './createViewport';

/**
 * Transform viewport coords to world coords.
 * @param viewport
 * @param origin viewport coords
 * @returns world coords
 */
const viewportProjection = (viewport: Viewport, origin: Vector<2>): Vector<2> =>
  add(
    viewport.getPosition(),
    dot(
      hadam(viewport.getBounds(), sub(origin, [0.5, 0.5])),
      1 / viewport.getZoom(),
    ),
  );

export default viewportProjection;
