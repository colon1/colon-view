// import { Component, createResource } from 'solid-js';
// import '@common/components/global.scss';
// import Ellipsis from '@common/components/spinner/Ellipsis';
// import Flex from '@common/components/wrapper/Flex';
// import PixelMapPage from './features/pixelmap/PixelMapPage';
// import { API_ADDRESS } from './features/config/env';
// import { PixelMapInfo } from './features/pixelmap/types';

// const fetchPixelMapInfo = async (name: string): Promise<PixelMapInfo> =>
//   (await fetch(`${API_ADDRESS}/map/${name}/info`)).json();

// const App: Component = () => {
//   const [resourceInfo] = createResource(
//     window.location.pathname.slice(1) || 'main',
//     fetchPixelMapInfo,
//   );
//   return (
//     <>
//       {((info) =>
//         info ? (
//           <PixelMapPage getInfo={() => info} />
//         ) : (
//           <Flex center style={{ position: 'fixed', height: '100%' }}>
//             <Ellipsis dotColor="#bcc1c9" />
//           </Flex>
//         ))(resourceInfo())}
//     </>
//   );
// };

// export default App;

import { Component } from 'solid-js';
import '@common/components/global.scss';
import Board from './features/board/Board';

const App: Component = () => {
  return <Board />;
};

export default App;
