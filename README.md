# 🐱‍💻 Development environment

## 💡 Before you start coding

[Helpful dev tools](https://youtu.be/-atblwgc63E) : [VS Code](https://code.visualstudio.com/) ; [Fira Code](https://github.com/tonsky/FiraCode) ; [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-manual) ; [Docker Desktop](https://docs.docker.com/desktop/windows/install/) ; [powerlevel10k](https://github.com/romkatv/powerlevel10k)

**VS Code Extensions :** ms-vscode-remote.remote-wsl ; ms-azuretools.vscode-docker ; ms-kubernetes-tools.vscode-kubernetes-tools ; ipedrazas.kubernetes-snippets ; editorconfig.editorconfig ; redhat.vscode-yaml ; esbenp.prettier-vscode ; dbaeumer.vscode-eslint ; eamodio.gitlens ; coenraads.bracket-pair-colorizer-2

## 🐱‍👤 Let's run the view

**Requirements :** [Yarn](https://yarnpkg.com/)

Copy `.env.example`, rename it to `.env`, update its values.

Install node dependencies : `yarn`

Run the server : `yarn run dev --host`

## 🐱‍🚀 You've to go deeper

### Test mobile devices

Make sure both the host and the mobile are connected to the same (WiFi) network.

Update the host firewalls to accpet incoming traffic on ports 3000, 4000 and 5000.

Update environment variable `VITE_API_ADDRESS` to `http://<HOST_IP_ADDRESS>:4000`

Proxy the incoming traffic to the correct address and ports :

```bash
netsh interface portproxy add v4tov4 listenport=3000 listenaddress=0.0.0.0 connectport=3000 connectaddress=<VM_IP_ADDRESS>
netsh interface portproxy add v4tov4 listenport=4000 listenaddress=0.0.0.0 connectport=4000 connectaddress=<VM_IP_ADDRESS>
netsh interface portproxy add v4tov4 listenport=4001 listenaddress=0.0.0.0 connectport=4001 connectaddress=<VM_IP_ADDRESS>
netsh interface portproxy add v4tov4 listenport=5000 listenaddress=0.0.0.0 connectport=5000 connectaddress=<VM_IP_ADDRESS>
```

Run the server (dev or prod mode) using the `--host` option.

#### Memo

Host IP address from a Windows terminal : `ipconfig`

VM IP address from the WSL terminal : `ip addr | grep eth0`

### Test production builds

Instead of running the server in dev mode, you can run it in prod mode :

Build the view : `yarn run build`

Run the server : `yarn run serve --host`
